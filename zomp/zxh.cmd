REM Prepare the environment for ZX and launch it

set ZOMP_DIR="%LOCALAPPDATA%\zomp"
set /P VERSION=<"%ZOMP_DIR%\etc\version.txt"
set ZX_DIR="%ZOMP_DIR%\lib\otpr\zx\%VERSION%"
pushd %ZX_DIR%
escript.exe make_zx
popd
erl.exe -pa "%ZX_DIR%/ebin" -run zx run "%*"
