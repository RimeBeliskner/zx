%%% @doc
%%% ZX Local
%%%
%%% This module defines procedures that affect the local host and terminate on
%%% completion.
%%% @end

-module(zx_local).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([initialize/0, set_version/1,
         list_realms/0, list_packages/1, list_versions/1,
         latest/1,
         set_dep/1, list_deps/0, list_deps/1, drop_dep/1, verup/1, package/1,
         update_app_file/0,
         import_realm/1, drop_realm/1,
         takeover/1, abdicate/1, set_timeout/1, add_mirror/0, drop_mirror/0,
         create_project/0, create_plt/0, dialyze/0,
         grow_a_pair/0, grow_a_pair/2, drop_key/1,
         create_user/0, create_userfile/0, export_user/0, import_user/1,
         create_realm/0, create_realmfile/0, create_realmfile/1]).

-export([select_user/1, select_private_key/1]).

-include("zx_logger.hrl").


-record(user_data,
        {realm        = none :: none | zx:realm(),
         username     = none :: none | zx:user_name(),
         realname     = none :: none | string(),
         contact_info = none :: none | [zx:contact_info()],
         keys         = none :: none | [zx:key_name()]}).

-record(realm_init,
        {realm = none :: none | zx:realm(),
         addr  = none :: none | string() | inet:ip_address(),
         port  = none :: none | inet:port_number(),
         sysop = none :: none | #user_data{},
         url   = none :: none | string()}).

-record(project,
        {type      = none :: none | app | lib | escript,
         license   = none :: none | none | undefined | string(),
         name      = none :: none | string(),
         id        = none :: none | zx:package_id() | zx:name(),
         prefix    = none :: none | zx:lower0_9(),
         appmod    = none :: none | zx:lower0_9(),
         module    = none :: none | zx:lower0_9(),
         author    = none :: none | string(),
         a_email   = none :: none | string(),
         copyright = none :: none | string(),
         c_email   = none :: none | string(),
         deps      = []   :: [zx:package_id()]}).



%%% Functions

-spec initialize() -> zx:outcome().
%% @private
%% Initialize a project in the local directory based on user input.

initialize() ->
    case filelib:is_file("zomp.meta") of
        false -> initialize(#project{});
        true  -> {error, "This project is already Zompified.", 17}
    end.

initialize(P = #project{type = none}) ->
    initialize(P#project{type = ask_project_type()});
initialize(P = #project{type = escript}) ->
    ok = log(warning, "escripts cannot be initialized."),
    initialize(P#project{type = ask_project_type()});
initialize(P = #project{type = app, id = none}) ->
    ID = {_, AppMod, _} = ask_package_id(),
    initialize(P#project{id = ID, appmod = AppMod});
initialize(P = #project{id = none}) ->
    initialize(P#project{id = ask_package_id()});
initialize(P = #project{prefix = none}) ->
    initialize(P#project{prefix = ask_prefix()});
initialize(P = #project{type = app, appmod = none}) ->
    initialize(P#project{appmod = ask_appmod()});
initialize(P = #project{type   = app,
                        id     = ID,
                        prefix = Prefix,
                        appmod = AppMod}) ->
    {ok, PS} = zx_lib:package_string(ID),
    Instructions =
        "~nAPPLICATION DATA CONFIRMATION~n"
        "[1] Type        : application~n"
        "[2] Package ID  : ~ts~n"
        "[3] Prefix      : ~ts~n"
        "[4] AppMod      : ~ts~n"
        "Press a number to select something to change, or [ENTER] to continue.~n",
    ok = io:format(Instructions, [PS, Prefix, AppMod]),
    case zx_tty:get_input() of
        "1" -> initialize(P#project{type   = none});
        "2" -> initialize(P#project{id     = none});
        "3" -> initialize(P#project{prefix = none});
        "4" -> initialize(P#project{appmod = none});
        ""  -> initialize_check(P);
        _   ->
            ok = zx_tty:derp(),
            initialize(P)
    end;
initialize(P = #project{type   = lib,
                        id     = ID,
                        prefix = Prefix}) ->
    {ok, PS} = zx_lib:package_string(ID),
    Instructions =
        "~nLIBRARY DATA CONFIRMATION~n"
        "[1] Type        : library~n"
        "[2] Package ID  : ~ts~n"
        "[3] Prefix      : ~ts~n"
        "Press a number to select something to change, or [ENTER] to continue.~n",
    ok = io:format(Instructions, [PS, Prefix]),
    case zx_tty:get_input() of
        "1" -> initialize(P#project{type      = none});
        "2" -> initialize(P#project{id        = none});
        "3" -> initialize(P#project{prefix    = none});
        ""  -> initialize_check(P);
        _   ->
            ok = zx_tty:derp(),
            initialize(P)
    end.


initialize_check(P = #project{id = PackageID}) ->
    case package_exists(PackageID) of
        false ->
            zompify(P);
        true ->
            Message =
                "~nDHOH!~n"
                "Sadly this package already exists.~n"
                "Two packages with the same name cannot exist in a single realm.~n"
                "Please pick another name.~n",
            ok = io:format(Message),
            initialize(P#project{id = none})
    end.


zompify(#project{type = Type, id = ID, prefix = Prefix, appmod = AM, deps = Deps}) ->
    ok = initialize_app_file(ID, AM),
    Data = #{package_id => ID, deps => Deps, type => Type, prefix => Prefix},
    ok = zx_lib:write_project_meta(Data),
    ok = ensure_emakefile(),
    {ok, PackageString} = zx_lib:package_string(ID),
    ok = log(info, "Project ~ts initialized.", [PackageString]),
    Message =
      "~nNOTICE:~n"
      "This project is currently listed as having no dependencies.~n"
      "If this is not true then run `zx set dep DepID` for each current dependency.~n"
      "(run `zx help` for more information on usage)~n",
    io:format(Message).


-spec ensure_emakefile() -> ok.

ensure_emakefile() ->
    case filelib:is_regular("Emakefile") of
        true ->
            ok;
        false ->
            Path = filename:join([os:getenv("ZX_DIR"), "templates", "Emakefile"]),
            {ok, _} = file:copy(Path, "Emakefile"),
            ok
    end.


-spec update_source_vsn(zx:version()) -> ok.
%% @private
%% Use grep to tell us which files have a `-vsn' attribute and which don't.
%% Use sed to insert a line after `-module' in the files that don't have it,
%% and update the files that do.
%% This procedure is still halfway into "works for me" territory. It will take a bit
%% of survey data to know which platforms can't work with some form of something
%% in here by default (bash may need to be called explicitly, for example, or the
%% old backtick executor terms used, etc).

update_source_vsn(Version) ->
    {ok, VersionString} = zx_lib:version_to_string(Version),
    AddF = "sed -i 's/^-module(.*)\\.$/&\\n-vsn(\"~s\")./' $(grep -L '^-vsn(' src/*)",
    SubF = "sed -i 's/-vsn(.*$/-vsn(\"~s\")./' $(grep -l '^-vsn(' src/*)",
    Add = lists:flatten(io_lib:format(AddF, [VersionString])),
    Sub = lists:flatten(io_lib:format(SubF, [VersionString])),
    _ = os:cmd(Add),
    _ = os:cmd(Sub),
    log(info, "Source version attributes set").


-spec update_app_vsn(zx:name(), zx:version()) -> ok.

update_app_vsn(Name, Version) ->
    {ok, VersionString} = zx_lib:version_to_string(Version),
    AppFile = filename:join("ebin", Name ++ ".app"),
    {ok, [{application, AppName, OldAppData}]} = file:consult(AppFile),
    NewAppData = lists:keystore(vsn, 1, OldAppData, {vsn, VersionString}),
    zx_lib:write_terms(AppFile, [{application, AppName, NewAppData}]).


-spec initialize_app_file(PackageID, AppMod) -> ok
    when PackageID :: zx:package_id(),
         AppMod    :: module().
%% @private
%% Update the app file or create it if it is missing.
%% TODO: If the app file is missing and an app/*.src exists, interpret that.

initialize_app_file({_, Name, Version}, AppMod) ->
    {ok, VersionString} = zx_lib:version_to_string(Version),
    AppName = list_to_atom(Name),
    AppFile = filename:join("ebin", Name ++ ".app"),
    {application, AppName, RawAppData} =
        case filelib:is_regular(AppFile) of
            true ->
                {ok, [D]} = file:consult(AppFile),
                D;
            false ->
                {application,
                 AppName,
                 [{registered,            []},
                  {included_applications, []},
                  {applications,          [stdlib, kernel]}]}
        end,
    Grep = "grep -oP '^-module\\(\\K[^)]+' src/* | cut -d: -f2",
    Modules = [list_to_atom(M) || M <- string:lexemes(os:cmd(Grep), "\n")],
    Properties =
        case (AppMod == none) or lists:keymember(mod, 1, RawAppData) of
            true ->
                [{vsn,     VersionString},
                 {modules, Modules}];
            false ->
                [{vsn,     VersionString},
                 {modules, Modules},
                 {mod,     {AppMod, []}}]
        end,
    Store = fun(T, L) -> lists:keystore(element(1, T), 1, L, T) end,
    AppData = lists:foldl(Store, RawAppData, Properties),
    AppProfile = {application, AppName, AppData},
    ok = log(info, "Writing app file: ~ts", [AppFile]),
    zx_lib:write_terms(AppFile, [AppProfile]).


-spec update_app_file() -> zx:outcome().

update_app_file() ->
    case zx_lib:read_project_meta() of
        {ok, Meta} -> update_app_file(Meta);
        Error      -> Error
    end.


update_app_file(Meta) ->
    {_, Name, Version} = maps:get(package_id, Meta),
    {ok, VersionString} = zx_lib:version_to_string(Version),
    AppName = list_to_atom(Name),
    AppFile = filename:join("ebin", Name ++ ".app"),
    {application, AppName, RawAppData} =
        case filelib:is_regular(AppFile) of
            true ->
                {ok, [D]} = file:consult(AppFile),
                D;
            false ->
                {application,
                 AppName,
                 [{registered,            []},
                  {included_applications, []},
                  {applications,          [stdlib, kernel]}]}
        end,
    Grep = "grep -oP '^-module\\(\\K[^)]+' src/* | cut -d: -f2",
    Modules = [list_to_atom(M) || M <- string:lexemes(os:cmd(Grep), "\n")],
    Properties = [{vsn, VersionString}, {modules, Modules}],
    Store = fun(T, L) -> lists:keystore(element(1, T), 1, L, T) end,
    AppData = lists:foldl(Store, RawAppData, Properties),
    AppProfile = {application, AppName, AppData},
    ok = zx_lib:write_terms(AppFile, [AppProfile]),
    log(info, "Writing app file: ~ts", [AppFile]).


-spec set_version(VersionString) -> zx:outcome()
    when VersionString :: string().
%% @private
%% Convert a version string to a new version, sanitizing it in the process and returning
%% a reasonable error message on bad input.

set_version(VersionString) ->
    case zx_lib:string_to_version(VersionString) of
        {error, invalid_version_string} ->
            Message = "Invalid version string: ~ts",
            {error, Message, 22};
        {ok, {_, _, z}} ->
            Message = "'set version' arguments must be complete, ex: 1.2.3",
            {error, Message, 22};
        {ok, NewVersion} ->
            {ok, Meta} = zx_lib:read_project_meta(),
            {Realm, Name, OldVersion} = maps:get(package_id, Meta),
            update_version(Realm, Name, OldVersion, NewVersion, Meta)
    end.


-spec update_version(Realm, Name, OldVersion, NewVersion, OldMeta) -> ok
    when Realm      :: zx:realm(),
         Name       :: zx:name(),
         OldVersion :: zx:version(),
         NewVersion :: zx:version(),
         OldMeta    :: zx:package_meta().
%% @private
%% Update a project's `zomp.meta' file by either incrementing the indicated component,
%% or setting the version number to the one specified in VersionString.
%% This part of the procedure updates the meta and does the final write, if the write
%% turns out to be possible. If successful it will indicate to the user what was
%% changed.

update_version(Realm, Name, OldVersion, NewVersion, OldMeta) ->
    PackageID = {Realm, Name, NewVersion},
    NewMeta = maps:put(package_id, PackageID, OldMeta),
    case zx_lib:write_project_meta(NewMeta) of
        ok ->
            ok = update_source_vsn(NewVersion),
            ok = update_app_vsn(Name, NewVersion),
            {ok, OldVS} = zx_lib:version_to_string(OldVersion),
            {ok, NewVS} = zx_lib:version_to_string(NewVersion),
            log(info, "Version changed from ~s to ~s.", [OldVS, NewVS]);
        Error ->
            ok = log(error, "Write to zomp.meta failed with: ~160tp", [Error]),
            Error
    end.


-spec list_realms() -> ok.
%% @private
%% List all currently configured realms.

list_realms() ->
    case zx_lib:list_realms() of
        []     -> io:format("Oh noes! No realms are configured!~n");
        Realms -> lists:foreach(fun(R) -> io:format("~ts~n", [R]) end, Realms)
    end.


-spec list_packages(zx:realm()) -> zx:outcome().
%% @private
%% Contact the indicated realm and query it for a list of registered packages and print
%% them to stdout.

list_packages(Realm) ->
    {ok, ID} = zx_daemon:list(Realm),
    case wait_result(ID) of
        {ok, []}           -> io:format("No packages available.~n");
        {ok, Packages}     -> lists:foreach(print_package(Realm), Packages);
        {error, bad_realm} -> {error, "Unconfigured realm or bad realm name.", 22};
        {error, timeout}   -> {error, "Request timed out.", 62};
        {error, network}   -> {error, "Network problem connecting to realm.", 101}
    end.

print_package(Realm) -> fun(Name) -> io:format("~ts-~ts~n", [Realm, Name]) end.


-spec list_versions(PackageName :: string()) -> zx:outcome().
%% @private
%% List the available versions of the package indicated. The user enters a string-form
%% package name (such as "otpr-zomp") and the return values will be full package strings
%% of the form "otpr-zomp-1.2.3", one per line printed to stdout.

list_versions(PackageString) ->
    case zx_lib:package_id(PackageString) of
        {ok, PackageID}                 -> list_versions2(PackageID);
        {error, invalid_package_string} -> {error, "Invalid package name.", 22}
    end.


list_versions2({Realm, Name, Version}) ->
    {ok, ID} = zx_daemon:list(Realm, Name, Version),
    case wait_result(ID) of
        {ok, []}             -> io:format("No versions available.~n");
        {ok, Versions}       -> lists:foreach(fun print_version/1, Versions);
        {error, bad_realm}   -> {error, "Bad realm name.", 22};
        {error, bad_package} -> {error, "Bad package name.", 22};
        {error, timeout}     -> {error, "Request timed out.", 62};
        {error, network}     -> {error, "Network problem connecting to realm.", 101}
    end.

print_version(Version) ->
    {ok, String} = zx_lib:version_to_string(Version),
    io:format("~ts~n", [String]).


-spec latest(PackageString :: string()) -> zx:outcome().

latest(PackageString) ->
    case zx_lib:package_id(PackageString) of
        {ok, PackageID}                 -> latest2(PackageID);
        {error, invalid_package_string} -> {error, "Invalid package name.", 22}
    end.

latest2(PackageID) ->
    {ok, ID} = zx_daemon:latest(PackageID),
    case wait_result(ID) of
        {ok, Version} -> print_version(Version);
        Error         -> Error
    end.


wait_result(ID) ->
    receive
        {result, ID, Result} -> Result
        after 5000           -> {error, timeout}
    end.


-spec import_realm(Path) -> zx:outcome()
    when Path :: file:filename().
%% @private
%% Configure the system for a new realm by interpreting a .zrf file.
%% Also log the SHA512 of the .zrf for the user.

import_realm(Path) ->
    case file:read_file(Path) of
        {ok, Data} ->
            Digest = crypto:hash(sha512, Data),
            Text = integer_to_list(binary:decode_unsigned(Digest, big), 16),
            ok = log(info, "SHA-512 of ~ts: ~ts", [Path, Text]),
            import_realm2(Data);
        {error, enoent} ->
            {error, "Realm bundle (.zrf) does not exist.", 2};
        {error, eisdir} ->
            {error, "Path is a directory, not a .zrf file.", 21}
    end.


-spec import_realm2(Data) -> zx:outcome()
    when Data :: binary().

import_realm2(Data) ->
    case zx_lib:b_to_t(Data) of
        {ok, {RealmConf, KeyDER}} ->
            Realm = maps:get(realm, RealmConf),
            ok = make_realm_dirs(Realm),
            ConfPath = zx_lib:realm_conf(Realm),
            ok = zx_lib:write_terms(ConfPath, maps:to_list(RealmConf)),
            KeyName = maps:get(key, RealmConf),
            KeyPath = zx_key:path(public, {Realm, KeyName}),
            ok = file:write_file(KeyPath, KeyDER),
            log(info, "Added realm ~ts.", [Realm]);
        error ->
            {error, "Invalid .zrf file.", 84}
    end.


-spec set_dep(PackageString :: string()) -> zx:outcome().
%% @private
%% Set a dependency in the current project. If the project currently has a dependency
%% on the same package then the version of that dependency is updated to reflect that
%% in the PackageString argument.

set_dep(PackageString) ->
    case zx_lib:package_id(PackageString) of
        {ok, {_, _, {_, _, z}}} ->
            Message =
                "Incomplete version tuple. Dependencies must be fully specified.",
            {error, Message, 22};
        {ok, PackageID} ->
            set_dep2(PackageID);
        {error, invalid_package_string} ->
            {error, "Invalid package string", 22}
    end.


-spec set_dep2(zx:package_id()) -> ok.

set_dep2(PackageID) ->
    {ok, Meta} = zx_lib:read_project_meta(),
    Deps = maps:get(deps, Meta),
    case lists:member(PackageID, Deps) of
        true  -> ok;
        false -> set_dep3(PackageID, Deps, Meta)
    end.


-spec set_dep3(PackageID, Deps, Meta) -> ok
    when PackageID :: zx:package_id(),
         Deps      :: [zx:package_id()],
         Meta      :: [term()].
%% @private
%% Given the PackageID, list of Deps and the current contents of the project Meta, add
%% or update Deps to include (or update) Deps to reflect a dependency on PackageID, if
%% such a dependency is not already present. Then write the project meta back to its
%% file and exit.

set_dep3(PackageID = {Realm, Name, NewVersion}, Deps, Meta) ->
    ExistingPackageIDs = fun({R, N, _}) -> {R, N} == {Realm, Name} end,
    NewDeps =
        case lists:partition(ExistingPackageIDs, Deps) of
            {[{Realm, Name, OldVersion}], Rest} ->
                Message = "Updating dep ~ts to ~ts",
                {ok, OldPS} = zx_lib:package_string({Realm, Name, OldVersion}),
                {ok, NewPS} = zx_lib:package_string({Realm, Name, NewVersion}),
                ok = log(info, Message, [OldPS, NewPS]),
                [PackageID | Rest];
            {[], Deps} ->
                {ok, PackageString} = zx_lib:package_string(PackageID),
                ok = log(info, "Adding dep ~ts", [PackageString]),
                [PackageID | Deps]
        end,
    NewMeta = maps:put(deps, NewDeps, Meta),
    zx_lib:write_project_meta(NewMeta).


-spec list_deps() -> zx:outcome().
%% @private
%% List deps in the current (local) project directory.
%% This is very different from list_deps/1, which makes a remote query to a Zomp
%% realm to discover deps for potentially unknown or locally uncached packages.

list_deps() ->
    case zx_lib:read_project_meta() of
        {ok, Meta} ->
            Deps = maps:get(deps, Meta),
            Print =
                fun(P) ->
                    {ok, PackageString} = zx_lib:package_string(P),
                    io:format("~ts~n", [PackageString])
                end,
            lists:foreach(Print, Deps);
        Error ->
            Error
    end.


-spec list_deps(zx:package_id()) -> zx:outcome().
%% @private
%% List deps for the specified package. This quite often requires a query to a Zomp
%% realm over the network, but not if the referenced package happens to be cached
%% locally.

list_deps(PackageString) ->
    case zx_lib:package_id(PackageString) of
        {ok, {_, _, {_, _, z}}} ->
            {error, "Packages must be fully specified; no partial versions.", 22};
        {ok, PackageID} ->
            log(info, "Phooey! list_deps(~16tp) isn't yet implemented!", [PackageID]);
        {error, invalid_package_string} ->
            {error, "Invalid package string.", 22}
    end.


-spec drop_dep(zx:package_id()) -> ok.
%% @private
%% Remove the indicate dependency from the local project's zomp.meta record.

drop_dep(PackageID) ->
    {ok, PackageString} = zx_lib:package_string(PackageID),
    Meta =
        case zx_lib:read_project_meta() of
            {ok, M} ->
                M;
            {error, enoent} ->
                ok = log(error, "zomp.meta not found. Is this a project directory?"),
                halt(1)
        end,
    Deps = maps:get(deps, Meta),
    case lists:member(PackageID, Deps) of
        true ->
            NewDeps = lists:delete(PackageID, Deps),
            NewMeta = maps:put(deps, NewDeps, Meta),
            ok = zx_lib:write_project_meta(NewMeta),
            Message = "~ts removed from dependencies.",
            log(info, Message, [PackageString]);
        false ->
            log(info, "~ts is not a dependency.", [PackageString])
    end.


-spec verup(Level) -> zx:outcome()
    when Level :: string().
%% @private
%% Convert input string arguments to acceptable atoms for use in update_version/1.

verup("major") -> version_up(major);
verup("minor") -> version_up(minor);
verup("patch") -> version_up(patch);
verup(_)       -> zx:usage_exit(22).


-spec version_up(Level) -> zx:outcome()
    when Level :: major
                | minor
                | patch.
%% @private
%% Update a project's `zomp.meta' file by either incrementing the indicated component,
%% or setting the version number to the one specified in VersionString.
%% This part of the procedure guards for the case when the zomp.meta file cannot be
%% read for some reason.

version_up(Arg) ->
    case zx_lib:read_project_meta() of
        {ok, Meta} ->
            PackageID = maps:get(package_id, Meta),
            version_up(Arg, PackageID, Meta);
        Error ->
            Error
    end.
    

-spec version_up(Level, PackageID, Meta) -> ok
    when Level     :: major
                    | minor
                    | patch
                    | zx:version(),
         PackageID :: zx:package_id(),
         Meta      :: [{atom(), term()}].
%% @private
%% Update a project's `zomp.meta' file by either incrementing the indicated component,
%% or setting the version number to the one specified in VersionString.
%% This part of the procedure does the actual update calculation, to include calling to
%% convert the VersionString (if it is passed) to a `version()' type and check its
%% validity (or halt if it is a bad string).

version_up(major, {Realm, Name, OldVersion = {Major, _, _}}, OldMeta) ->
    NewVersion = {Major + 1, 0, 0},
    update_version(Realm, Name, OldVersion, NewVersion, OldMeta);
version_up(minor, {Realm, Name, OldVersion = {Major, Minor, _}}, OldMeta) ->
    NewVersion = {Major, Minor + 1, 0},
    update_version(Realm, Name, OldVersion, NewVersion, OldMeta);
version_up(patch, {Realm, Name, OldVersion = {Major, Minor, Patch}}, OldMeta) ->
    NewVersion = {Major, Minor, Patch + 1},
    update_version(Realm, Name, OldVersion, NewVersion, OldMeta).


-spec package(TargetDir) -> no_return()
    when TargetDir :: file:filename().
%% @private
%% Turn a target project directory into a package, prompting the user for appropriate
%% key selection or generation actions along the way.
%% TODO: Add user selection in the case more than one user with private keys exists.

package(TargetDir) ->
    ok = log(info, "Packaging ~ts", [TargetDir]),
    {ok, Meta} = zx_lib:read_project_meta(TargetDir),
    PackageID = {Realm, _, _} = maps:get(package_id, Meta),
    Deps = maps:get(deps, Meta),
    UserName = select_user(Realm),
    KeyName = select_private_key({Realm, UserName}),
    ok = log(info, "Using key: ~ts/~ts", [Realm, KeyName]),
    {ok, PackageString} = zx_lib:package_string(PackageID),
    ZspFile = PackageString ++ ".zsp",
    case filelib:is_regular(ZspFile) of
        false -> package2(TargetDir, PackageID, Deps, KeyName, ZspFile);
        true  -> {error, "Package file already exists. Aborting", 17}
    end.

package2(TargetDir, PackageID, Deps, KeyName, ZspFile) ->
    ok = remove_binaries(TargetDir),
    CrashDump = filename:join(TargetDir, "erl_crash.dump"),
    ok =
        case filelib:is_regular(CrashDump) of
            true  -> file:delete(CrashDump);
            false -> ok
        end,
    {ok, Everything} = file:list_dir(TargetDir),
    DotFiles = filelib:wildcard(".*", TargetDir),
    Targets = lists:subtract(Everything, DotFiles),
    {ok, CWD} = file:get_cwd(),
    ok = file:set_cwd(TargetDir),
    Grep = "grep -oP '^-module\\(\\K[^)]+' src/* | cut -d: -f2",
    Modules = string:lexemes(os:cmd(Grep), "\n"),
    TarGzPath = filename:join(zx_lib:path(tmp), ZspFile ++ ".tgz"),
    ok = erl_tar:create(TarGzPath, Targets, [compressed]),
    {ok, TgzBin} = file:read_file(TarGzPath),
    ok = file:delete(TarGzPath),
    MetaBin = term_to_binary({PackageID, KeyName, Deps, Modules}),
    MetaSize = byte_size(MetaBin),
    SignMe = <<MetaSize:16, MetaBin:MetaSize/binary, TgzBin/binary>>,
    {ok, Key} = zx_key:load(private, {element(1, PackageID), KeyName}),
    Sig = public_key:sign(SignMe, sha512, Key),
    SigSize = byte_size(Sig),
    ZspData = <<SigSize:24, Sig:SigSize/binary, SignMe/binary>>,
    ok = file:set_cwd(CWD),
    ok = file:write_file(ZspFile, ZspData),
    log(info, "Wrote archive ~ts", [ZspFile]).


-spec remove_binaries(TargetDir) -> ok
    when TargetDir :: file:filename().
%% @private
%% Procedure to delete all .beam and .ez files from a given directory starting at
%% TargetDir. Called as part of the pre-packaging sanitization procedure.

remove_binaries(TargetDir) ->
    Beams = filelib:wildcard("**/*.{beam,ez}", TargetDir),
    ToDelete = [filename:join(TargetDir, Beam) || Beam <- Beams],
    lists:foreach(fun file:delete/1, ToDelete).


-spec create_project() -> ok.
%% @private
%% Interact with the user to determine what kind of project they want, and template
%% it based on the outcome.

create_project() ->
    create(#project{}).

create(P = #project{type = none}) ->
    create(P#project{type = ask_project_type()});
create(P = #project{name = none}) ->
    create(P#project{name = ask_project_name()});
create(P = #project{type = escript, id = none}) ->
    create(P#project{id = ask_script_name()});
create(P = #project{type = app, id = none}) ->
    ID = {_, AppMod, _} = ask_package_id(),
    create(P#project{id = ID, appmod = list_to_atom(AppMod)});
create(P = #project{type = lib, id = none}) ->
    ID = {_, Module, _} = ask_package_id(),
    create(P#project{id = ID, module = list_to_atom(Module)});
create(P = #project{type = app, prefix = none}) ->
    create(P#project{prefix = ask_prefix()});
create(P = #project{type = lib, prefix = none}) ->
    create(P#project{prefix = ask_prefix()});
create(P = #project{type = app, appmod = none}) ->
    create(P#project{appmod = ask_appmod()});
create(P = #project{type = lib, module = none}) ->
    create(P#project{module = ask_module()});
create(P = #project{author = none, copyright = none}) ->
    {A, E} = ask_author(),
    create(P#project{author = A, a_email = E, copyright = A, c_email = E});
create(P = #project{author = none}) ->
    {Author, Email} = ask_author(),
    create(P#project{author = Author, a_email = Email});
create(P = #project{a_email = none}) ->
    create(P#project{a_email = ask_email_optional()});
create(P = #project{copyright = none}) ->
    {Holder, Email} = ask_copyright(),
    create(P#project{copyright = Holder, c_email = Email});
create(P = #project{c_email = none}) ->
    create(P#project{c_email = ask_email_optional()});
create(P = #project{license = none}) ->
    create(P#project{license = ask_license()});
create(P = #project{type      = app,
                    license   = License,
                    name      = Name,
                    id        = ID,
                    prefix    = Prefix,
                    appmod    = AppMod,
                    author    = Author,
                    a_email   = AEmail,
                    copyright = CR,
                    c_email   = CEmail}) ->
    {ok, PS} = zx_lib:package_string(ID),
    Instructions =
        "~nAPPLICATION DATA CONFIRMATION~n"
        "[ 1] Type                    : application~n"
        "[ 2] Project Name            : ~ts~n"
        "[ 3] Package ID              : ~ts~n"
        "[ 4] Author                  : ~ts~n"
        "[ 5] Author's Email          : ~ts~n"
        "[ 6] Copyright Holder        : ~ts~n"
        "[ 7] Copyright Holder's Email: ~ts~n"
        "[ 8] License                 : ~ts~n"
        "[ 9] Prefix                  : ~ts~n"
        "[10] AppMod                  : ~tw~n"
        "Press a number to select something to change, or [ENTER] to continue.~n",
    ok = io:format(Instructions,
                   [Name, PS, Author, AEmail, CR, CEmail, License, Prefix, AppMod]),
    case zx_tty:get_input() of
        "1"  -> create(P#project{type      = none});
        "2"  -> create(P#project{name      = none});
        "3"  -> create(P#project{id        = none});
        "4"  -> create(P#project{author    = none});
        "5"  -> create(P#project{a_email   = none});
        "6"  -> create(P#project{copyright = none});
        "7"  -> create(P#project{c_email   = none});
        "8"  -> create(P#project{license   = none});
        "9"  -> create(P#project{prefix    = none});
        "10" -> create(P#project{appmod    = none});
        ""   -> create_check(P);
        _    ->
            ok = zx_tty:derp(),
            create(P)
    end;
create(P = #project{type      = lib,
                    license   = License,
                    name      = Name,
                    id        = ID,
                    prefix    = Prefix,
                    module    = Module,
                    author    = Author,
                    a_email   = AEmail,
                    copyright = CR,
                    c_email   = CEmail}) ->
    {ok, PS} = zx_lib:package_string(ID),
    Instructions =
        "~nLIBRARY DATA CONFIRMATION~n"
        "[ 1] Type                    : library~n"
        "[ 2] Project Name            : ~ts~n"
        "[ 3] Package ID              : ~ts~n"
        "[ 4] Author                  : ~ts~n"
        "[ 5] Author's Email          : ~ts~n"
        "[ 6] Copyright Holder        : ~ts~n"
        "[ 7] Copyright Holder's Email: ~ts~n"
        "[ 8] License                 : ~ts~n"
        "[ 9] Prefix                  : ~ts~n"
        "[10] Module                  : ~tw~n"
        "Press a number to select something to change, or [ENTER] to continue.~n",
    ok = io:format(Instructions,
                   [Name, PS, Author, AEmail, CR, CEmail, License, Prefix, Module]),
    case zx_tty:get_input() of
        "1"  -> create(P#project{type      = none});
        "2"  -> create(P#project{name      = none});
        "3"  -> create(P#project{id        = none});
        "4"  -> create(P#project{author    = none});
        "5"  -> create(P#project{a_email   = none});
        "6"  -> create(P#project{copyright = none});
        "7"  -> create(P#project{c_email   = none});
        "8"  -> create(P#project{license   = none});
        "9"  -> create(P#project{prefix    = none});
        "10" -> create(P#project{module    = none});
        ""  -> create_check(P);
        _   ->
            ok = zx_tty:derp(),
            create(P)
    end;
create(P = #project{type      = escript,
                    license   = License,
                    name      = Name,
                    id        = ID,
                    author    = Author,
                    a_email   = AEmail,
                    copyright = CR,
                    c_email   = CEmail}) ->
    Instructions =
        "~nLIBRARY DATA CONFIRMATION~n"
        "[1] Type                    : escript~n"
        "[2] Project Name            : ~ts~n"
        "[3] Script                  : ~ts~n"
        "[4] Author                  : ~ts~n"
        "[5] Author's Email          : ~ts~n"
        "[6] Copyright Holder        : ~ts~n"
        "[7] Copyright Holder's Email: ~ts~n"
        "[8] License                 : ~ts~n"
        "Press a number to select something to change, or [ENTER] to continue.~n",
    ok = io:format(Instructions, [Name, ID, Author, AEmail, CR, CEmail, License]),
    case zx_tty:get_input() of
        "1" -> create(P#project{type      = none});
        "2" -> create(P#project{name      = none});
        "3" -> create(P#project{id        = none});
        "4" -> create(P#project{author    = none});
        "5" -> create(P#project{a_email   = none});
        "6" -> create(P#project{copyright = none});
        "7" -> create(P#project{c_email   = none});
        "8" -> create(P#project{license   = none});
        ""  -> create_project(P);
        _   ->
            ok = zx_tty:derp(),
            create(P)
    end.


create_check(P = #project{id = PackageID}) ->
    case package_exists(PackageID) of
        false ->
            create_project(P);
        true ->
            Message =
                "~nDHOH!~n"
                "Sadly this package already exists.~n"
                "Two packages with the same name cannot exist in a single realm.~n"
                "Please pick another name.~n",
            ok = io:format(Message),
            create(P#project{id = none})
    end.


create_project(#project{type      = escript,
                        license   = Title,
                        name      = Name,
                        id        = File,
                        author    = Credit,
                        a_email   = AEmail,
                        copyright = Holder,
                        c_email   = CEmail}) ->
    Source = filename:join(os:getenv("ZX_DIR"), "templates/escript"),
    Substitutions =
        [{"〘\*PROJECT NAME\*〙", Name},
         {"〘\*SCRIPT\*〙",       script(File)},
         {"〘\*AUTHOR\*〙",       author(Credit, AEmail)},
         {"〘\*COPYRIGHT\*〙",    copyright(Holder, CEmail)},
         {"〘\*LICENSE\*〙",      license(Title)}],
    {ok, Raw} = file:read_file(Source),
    UTF8 = unicode:characters_to_list(Raw, utf8),
    Cooked = substitute(UTF8, Substitutions),
    ok = file:write_file(File, Cooked),
    Message =
        "Escript \"~ts\" written to ~ts.~n"
        "You may want to change the file mode to add \"execute\" permissions.",
    log(info, Message, [Name, File]);
create_project(P = #project{id = {_, Name, _}}) ->
    case file:make_dir(Name) of
        ok ->
            ok = file:set_cwd(Name),
            munge_sources(P),
            zompify(P);
        {error, Reason} ->
            Message = "Creating directory ~ts failed with ~tp. Aborting.",
            Info = io_lib:format(Message, [Name, Reason]),
            {error, Info, 1}
    end.


munge_sources(#project{type      = app,
                       name      = Name,
                       license   = Title,
                       prefix    = Prefix,
                       appmod    = AppMod,
                       author    = Credit,
                       a_email   = AEmail,
                       copyright = Holder,
                       c_email   = CEmail}) ->
    ok = file:make_dir("src"),
    ok = file:make_dir("ebin"),
    {ok, ProjectDir} = file:get_cwd(),
    Substitutions =
        [{"〘\*PROJECT NAME\*〙", Name},
         {"〘\*PREFIX\*〙",       Prefix},
         {"〘\*APP MOD\*〙",      atom_to_list(AppMod)},
         {"〘\*AUTHOR\*〙",       author(Credit, AEmail)},
         {"〘\*COPYRIGHT\*〙",    copyright(Holder, CEmail)},
         {"〘\*LICENSE\*〙",      license(Title)}],
    TemplateDir = filename:join([os:getenv("ZX_DIR"), "templates", "example_server"]),
    ok = file:set_cwd("src"),
    AppModFile = atom_to_list(AppMod) ++ ".erl",
    {ok, RawAppMod} = file:read_file(filename:join(TemplateDir, "appmod.erl")),
    UTF8AppMod = unicode:characters_to_list(RawAppMod, utf8),
    CookedAppMod = substitute(UTF8AppMod, Substitutions),
    ok = file:write_file(AppModFile, CookedAppMod),
    ModuleTemplates =
        ["sup.erl", "clients.erl", "client_man.erl", "client_sup.erl", "client.erl"],
    Manifest = [filename:join(TemplateDir, T) || T <- ModuleTemplates],
    ok = transform(Manifest, Prefix, Substitutions),
    file:set_cwd(ProjectDir);
munge_sources(#project{type      = lib,
                       name      = Name,
                       license   = Title,
                       module    = Module,
                       author    = Credit,
                       a_email   = AEmail,
                       copyright = Holder,
                       c_email   = CEmail}) ->
    ok = file:make_dir("src"),
    ok = file:make_dir("ebin"),
    {ok, ProjectDir} = file:get_cwd(),
    Substitutions =
        [{"〘\*PROJECT NAME\*〙", Name},
         {"〘\*MODULE\*〙",       atom_to_list(Module)},
         {"〘\*AUTHOR\*〙",       author(Credit, AEmail)},
         {"〘\*COPYRIGHT\*〙",    copyright(Holder, CEmail)},
         {"〘\*LICENSE\*〙",      license(Title)}],
    TemplateDir = filename:join([os:getenv("ZX_DIR"), "templates", "boringlib"]),
    ok = file:set_cwd("src"),
    ModFile = atom_to_list(Module) ++ ".erl",
    {ok, RawMod} = file:read_file(filename:join(TemplateDir, "funfile.erl")),
    UTF8Mod = unicode:characters_to_list(RawMod, utf8),
    CookedMod = substitute(UTF8Mod, Substitutions),
    ok = file:write_file(ModFile, CookedMod),
    file:set_cwd(ProjectDir).


transform([Template | Rest], Prefix, Substitutions) ->
    {ok, Raw} = file:read_file(Template),
    Data = substitute(Raw, Substitutions),
    Path = Prefix ++ filename:basename(Template),
    ok = file:write_file(Path, Data),
    transform(Rest, Prefix, Substitutions);
transform([], _, _) ->
    ok.


substitute(Raw, [{Pattern, Value} | Rest]) ->
    Cooking = string:replace(Raw, Pattern, Value, all),
    substitute(Cooking, Rest);
substitute(Cooked, []) ->
    unicode:characters_to_list(Cooked, utf8).


license("")              -> "";
license(Title)           -> "-license(\""++ Title ++ "\").".

script("")               -> "";
script(Name)             -> "-script(\"" ++ Name ++ "\").".

copyright("", "")        -> "";
copyright(Holder, "")    -> "-copyright(\"" ++ Holder ++ "\").";
copyright("", Email)     -> "-copyright(\"<" ++ Email ++ ">\").";
copyright(Holder, Email) -> "-copyright(\"" ++ Holder ++ " <" ++ Email ++ ">\").".

author("", "")           -> "";
author(Credit, "")       -> "-author(\"" ++ Credit ++ "\").";
author("", Email)        -> "-author(\"<" ++ Email ++ ">\").";
author(Credit, Email)    -> "-author(\"" ++ Credit ++ " <" ++ Email ++">\").".
    


-spec ask_project_type() -> app | lib | escript | no_return().
%% @private
%% Determine whether the user wants to create an app, a lib, or nothing.

ask_project_type() ->
    Instructions =
        "~nPROJECT TYPE~n"
        "Select a project type.~n"
        "Remember, an \"application\" in Erlang is anything that needs to be started "
        "to work, even if it acts in a supporting role.~n"
        "(Note that escripts cannot be initialized as packaged projects.~n"
        "[1] Application~n"
        "[2] Library~n"
        "[3] Escript~n",
    ok = io:format(Instructions),
    case zx_tty:get_input() of
        "1" -> app;
        "2" -> lib;
        "3" -> escript;
        _   ->
            ok = zx_tty:derp(),
            ask_project_type()
    end.


-spec ask_script_name() -> zx:lower0_9() | no_return().

ask_script_name() ->
    Instructions =
        "~nESCRIPT NAME~n"
        "Enter the name of your escript. This will be its callable filename.~n"
        "[a-z0-9_]*~n",
    case zx_tty:get_lower0_9(Instructions) of
        "" ->
            ok = io:format("The script has to be called something. Try again. ~n"),
            ask_script_name();
        Script ->
            Script
    end.


-spec ask_package_id() -> zx:package_id() | no_return().

ask_package_id() ->
    Instructions =
        "~nPACKAGE ID~n"
        "A package ID is a set of three strings separated by dashes that represent "
        "a project's realm, package name, and version number.~n"
        "Package IDs typically look like this: \"otpr-my_project-1.2.3\" or similar.~n"
        "Omitted realms default to \"otpr\". Omitted versions default to \"0.1.0\".~n"
        "(A formal definition is available here: "
        "http://zxq9.com/projects/zomp/zx_usage.en.html#identifiers)~n",
    RawPackageString = zx_tty:get_input(Instructions),
    case zx_lib:package_id(RawPackageString) of
        {ok, {R, N, {z, z, z}}}         -> {R, N, {0, 1, 0}};
        {ok, {R, N, {X, z, z}}}         -> {R, N, {X, 0, 0}};
        {ok, {R, N, {X, Y, z}}}         -> {R, N, {X, Y, 0}};
        {ok, ID}                        -> ID;
        {error, invalid_package_string} ->
            Message = "Sorry, \"~tp\" is an invalid package ID. Try again.~n",
            ok = io:format(Message, [RawPackageString]),
            ask_package_id()
    end.


-spec ask_prefix() -> string().
%% @private
%% Get a valid module prefix to use as a namespace for new modules.

ask_prefix() ->
    Instructions =
        "~nPICKING A PREFIX~n"
        "Most projects use a prefix with a trailing underscore to namespace their "
        "modules. For example, example_server uses the module prefix \"es_\" "
        "internally.~n"
        "This is optional.~n",
    case zx_tty:get_input(Instructions, [], "[ENTER] to leave blank") of
        "" ->
            "";
        String ->
            case zx_lib:valid_lower0_9(String) of
                true ->
                    String;
                false ->
                    Message = "The string \"~ts\" isn't valid. Try \"[a-z0-9_]*\".~n",
                    ok = io:format(Message, [String]),
                    ask_prefix()
            end
    end.


-spec ask_appmod() -> atom() | no_return().

ask_appmod() ->
    Instructions =
        "~nAPPLICATION MODULE~n"
        "Enter the name of your main/start interface module where the application "
        "callback start/2 has been defined.~n",
    list_to_atom(zx_tty:get_lower0_9(Instructions)).


-spec ask_module() -> atom() | no_return().

ask_module() ->
    Instructions =
        "~nINTERFACE MODULE~n"
        "Enter the name of the library's initial interface module.~n",
    list_to_atom(zx_tty:get_lower0_9(Instructions)).


-spec package_exists(zx:package_id()) -> boolean().
%% @private
%% TODO: Change this from a stub into a for-real check via zx_daemon.
%% Check the realm's upstream for the existence of a package that already has the same
%% name, or the name of any modules in src/ and report them to the user. Give the user
%% a chance to change their package's name or ignore the conflict, and report all module
%% naming conflicts.

package_exists(_) ->
    false.


-spec create_plt() -> ok.
%% @private
%% Build a general plt file for Dialyzer based on the core Erland distro.
%% TODO: Make a per-package + dependencies version of this.
%% TODO: PLT build options.
%% TODO: Meaningful failure messages.

create_plt() ->
    PLT = default_plt(),
    Template =
        "dialyzer --build_plt"
                " --output_plt ~ts"
                " --apps asn1 reltool wx common_test crypto erts eunit inets"
                       " kernel mnesia public_key sasl ssh ssl stdlib",
    Command = io_lib:format(Template, [PLT]),
    Message =
        "Generating PLT file and writing to: ~ts~n"
        "    There will be a list of \"unknown functions\" in the final output.~n"
        "    Don't panic. This is normal. Turtles all the way down, after all...",
    ok = log(info, Message, [PLT]),
    ok = log(info, "This may take a while. Patience is a virtue."),
    Out = os:cmd(Command),
    log(info, Out).


-spec default_plt() -> file:filename().

default_plt() ->
    filename:join(zx_lib:zomp_dir(), "basic.plt").


-spec dialyze() -> ok.
%% @private
%% Preps a copy of this script for typechecking with Dialyzer.
%% TODO: Create a package_id() based version of this to handle dialyzation of complex
%%       projects.

dialyze() ->
    PLT = default_plt(),
    ok =
        case filelib:is_regular(PLT) of
            true  -> log(info, "Using PLT: ~ts", [PLT]);
            false -> create_plt()
        end,
    Me = escript:script_name(),
    EvilTwin = filename:join(zx_lib:path(tmp), filename:basename(Me ++ ".erl")),
    ok = log(info, "Temporarily reconstructing ~ts as ~ts", [Me, EvilTwin]),
    Sed = io_lib:format("sed 's/^#!.*$//' ~s > ~s", [Me, EvilTwin]),
    ok = zx_lib:exec_shell(Sed),
    ok = case dialyzer:run([{init_plt, PLT}, {from, src_code}, {files, [EvilTwin]}]) of
        [] ->
            io:format("Dialyzer found no errors and returned no warnings! Yay!~n");
        Warnings ->
            Mine = [dialyzer:format_warning({Tag, {Me, Line}, Msg})
                    || {Tag, {_, Line}, Msg} <- Warnings],
            lists:foreach(fun io:format/1, Mine)
    end,
    file:delete(EvilTwin).


-spec grow_a_pair() -> zx:outcome().
%% @private
%% Execute the key generation procedure for 16k RSA keys once and then terminate.

grow_a_pair() ->
    case select_realm() of
        error -> {error, "No realms configured.", 61};
        Realm -> grow_a_pair(Realm)
    end.


grow_a_pair(Realm) ->
    UserName = select_user(Realm),
    grow_a_pair(Realm, UserName).


grow_a_pair(Realm, UserName) ->
    KeyTag = zx_key:prompt_keygen(),
    KeyName = UserName ++ "-" ++ KeyTag,
    case zx_key:generate_rsa({Realm, KeyName}) of
        ok ->
            ok;
        {error, exists} ->
            ok = io:format("That key name already exists. Try something different.~n"),
            grow_a_pair(Realm, UserName);
        Error ->
            Error
    end.


-spec drop_key(zx:key_id()) -> ok.
%% @private
%% Given a KeyID, remove the related public and private keys from the keystore, if they
%% exist. If not, exit with a message that no keys were found, but do not return an
%% error exit value (this instruction is idempotent if used in shell scripts).

drop_key({Realm, KeyName}) ->
    Pattern = filename:join(zx_lib:path(key, Realm), KeyName ++ ".{key,pub}.der"),
    case filelib:wildcard(Pattern) of
        [] ->
            log(warning, "Key ~ts/~ts not found", [Realm, KeyName]);
        Files ->
            ok = lists:foreach(fun file:delete/1, Files),
            log(info, "Keyset ~ts/~ts removed", [Realm, KeyName])
    end.


-spec create_realm() -> ok.
%% @private
%% Prompt the user to input the information necessary to create a new zomp realm,
%% package the data appropriately for the server and deliver the final keys and
%% realm file to the user.

create_realm() ->
    ok = log(info, "WOOHOO! Making a new realm!"),
    create_realm(#realm_init{}).


create_realm(R = #realm_init{realm = none}) ->
    create_realm(R#realm_init{realm = ask_realm()});
create_realm(R = #realm_init{addr  = none}) ->
    create_realm(R#realm_init{addr = ask_addr()});
create_realm(R = #realm_init{port  = none}) ->
    create_realm(R#realm_init{port = ask_port()});
create_realm(R = #realm_init{url = none}) ->
    create_realm(R#realm_init{url = ask_url()});
create_realm(R = #realm_init{realm = Realm, sysop = none}) ->
    create_realm(R#realm_init{sysop = create_sysop(#user_data{realm = Realm})});
create_realm(R = #realm_init{realm = Realm, addr = Addr, port = Port, url = URL}) ->
    Instructions =
        "~nREALM DATA CONFIRMATION~n"
        "That's everything! Please confirm these settings:~n"
        "[1] Realm Name  : ~ts~n"
        "[2] Host Address: ~ts~n"
        "[3] Port Number : ~w~n"
        "[4] URL         : ~ts~n"
        "Press a number to select something to change, or [ENTER] to continue.~n",
    ok = io:format(Instructions, [Realm, stringify_address(Addr), Port, URL]),
    case zx_tty:get_input() of
        "1" -> create_realm(R#realm_init{realm = none});
        "2" -> create_realm(R#realm_init{addr  = none});
        "3" -> create_realm(R#realm_init{port  = none});
        "4" -> create_realm(R#realm_init{url   = none});
        ""  -> store_realm(R);
        _   ->
            ok = zx_tty:derp(),
            create_realm(R)
    end.


store_realm(#realm_init{realm = Realm,
                        addr  = Addr,
                        port  = Port,
                        url   = URL,
                        sysop = Sysop = #user_data{username = UserName,
                                                   keys     = [KeyName]}}) ->
    ok = make_realm_dirs(Realm),
    RealmConf =
        [{realm, Realm},
         {prime, {Addr, Port}},
         {sysop, UserName},
         {key,   KeyName},
         {url,   URL}],
    ok = store_user(Sysop),
    RealmConfPath = filename:join(zx_lib:path(etc, Realm), "realm.conf"),
    ok = zx_lib:write_terms(RealmConfPath, RealmConf),
    ok = create_realmfile(Realm),
    ok = create_userfile(Realm, UserName),
    ZPUF = Realm ++ "-" ++ UserName ++ ".zpuf",
    ZRF = Realm ++ ".zrf",
    Message =
      "=============================================================================~n"
      "DONE!~n"
      "The realm ~ts has been created and is accessible from this system.~n"
      "~n"
      "Realm configuration file has been written to:  ~ts~n"
      "Sysop userfile has been written to:            ~ts~n"
      "~n"
      "-----------------------------------------------------------------------------~n"
      "NODE AND CLIENT CONFIGURATION~n"
      "~n"
      "PRIME NODE configuration requires three commands:~n"
      "1. Configure the realm:    `zx import realm ~ts`~n"
      "2. Add the sysop user:     `zx import user ~ts`~n"
      "3. Become the prime node:  `zx takeover ~ts`~n"
      "~n"
      "ZX CLIENT and ZOMP DISTRIBUTION NODE configuration requires one command:~n"
      "1. Configure the realm:   `zx add realm ~ts`~n"
      "~n"
      "How to distribute ~ts is up to you.~n"
      "=============================================================================~n",
    Substitutions = [Realm, ZRF, ZPUF, ZRF, ZPUF, Realm, ZRF, ZRF],
    io:format(Message, Substitutions).


-spec ask_realm() -> string().

ask_realm() ->
    Instructions =
        "~nNAMING~n"
        "Enter a name for your new realm.~n"
        "Names can contain only lower-case letters, numbers and the underscore.~n"
        "Names must begin with a lower-case letter.~n",
    Realm = zx_tty:get_lower0_9(Instructions),
    case realm_exists(Realm) of
        false ->
            Realm;
        true ->
            ok = io:format("That realm already exists. Be more original.~n"),
            ask_realm()
    end.


-spec ask_addr() -> string().

ask_addr() ->
    Message =
        "~nHOST ADDRESS~n"
        "Enter the hostname, IPv4 or IPv6 address of the realm's prime Zomp node.~n"
        "DO NOT INCLUDE A PORT NUMBER IN THIS STEP~n",
    ok = io:format(Message),
    case zx_tty:get_input() of
        ""    ->
            ok = io:format("You need to enter an address.~n"),
            ask_addr();
        String ->
            parse_maybe_address(String)
    end.


-spec parse_maybe_address(string()) -> inet:hostname() | inet:ip_address().

parse_maybe_address(String) ->
    case inet:parse_address(String) of
        {ok, Address}   -> Address;
        {error, einval} -> String
    end.


-spec stringify_address(inet:hostname() | inet:ip_address()) -> string().

stringify_address(Address) ->
    case inet:ntoa(Address) of
        {error, einval} -> Address;
        String          -> String
    end.


-spec ask_port() -> inet:port_number().

ask_port() ->
    Message =
        "~nPUBLIC PORT NUMBER~n"
        "Enter the publicly visible (external) port number at which this service "
        "should be available.~n"
        "(This might be different from the local port number if you are forwarding "
        "ports or have a complex network layout.)~n",
    ok = io:format(Message),
    prompt_port_number(11311).


-spec prompt_port_number(Current) -> Result
    when Current :: inet:port_number(),
         Result  :: inet:port_number().

prompt_port_number(Current) ->
    ok = io:format("A valid port is any number from 1 to 65535.~n"),
    case zx_tty:get_input("[~w]", [Current]) of
        "" ->
            Current;
        S ->
            try
                case list_to_integer(S) of
                    Port when 16#ffff >= Port, Port > 0 ->
                        Port;
                    Illegal ->
                        Whoops = "Whoops! ~w is out of bounds (1~65535). Try again.~n",
                        ok = io:format(Whoops, [Illegal]),
                        prompt_port_number(Current)
                end
            catch error:badarg ->
                ok = io:format("~160tp is not a port number. Try again...", [S]),
                prompt_port_number(Current)
            end
    end.


-spec ask_url() -> string().

ask_url() ->
    Message =
        "~nURL~n"
        "Most public realms have a website, IRC channel, or similar location where its "
        "community (or customers) communicate. If you have such a URL enter it here.~n"
        "NOTE: No checking is performed on the input here. Confuse your users at "
        "your own peril!~n"
        "[ENTER] to leave blank.~n",
    zx_tty:get_input(Message).


-spec create_sysop(InitUser) -> FullUser
    when InitUser :: #user_data{},
         FullUser :: #user_data{}.

create_sysop(U = #user_data{username = none}) ->
    UserName = ask_username(),
    KeyName = UserName ++ "-root",
    create_sysop(U#user_data{username = UserName, keys = [KeyName]});
create_sysop(U = #user_data{realname = none}) ->
    create_sysop(U#user_data{realname = ask_realname()});
create_sysop(U = #user_data{contact_info = none}) ->
    create_sysop(U#user_data{contact_info = [{"email", ask_email()}]});
create_sysop(U = #user_data{username     = UserName,
                            realname     = RealName,
                            contact_info = [{"email", Email}]}) ->
    Instructions =
        "~nSYSOP DATA CONFIRMATION~n"
        "Please correct or confirm the sysop's data:~n"
        "[1] Username : ~ts~n"
        "[2] Real Name: ~ts~n"
        "[3] Email    : ~ts~n"
        "Press a number to select something to change, or [ENTER] to accept.~n",
    ok = io:format(Instructions, [UserName, RealName, Email]),
    case zx_tty:get_input() of
        "1" -> create_sysop(U#user_data{username     = none});
        "2" -> create_sysop(U#user_data{realname     = none});
        "3" -> create_sysop(U#user_data{contact_info = none});
        ""  -> U;
        _   ->
            ok = zx_tty:derp(),
            create_sysop(U)
    end.


-spec create_user() -> ok.

create_user() ->
    UserName = create_user(#user_data{}),
    log(info, "User ~ts created.", [UserName]).


-spec create_user(#user_data{}) -> zx:user_name().

create_user(U = #user_data{realm = none}) ->
    case pick_realm() of
        {ok, Realm}        -> create_user(U#user_data{realm = Realm});
        {error, no_realms} -> {error, "No realms configured.", 1}
    end;
create_user(U = #user_data{username = none}) ->
    UserName = ask_username(),
    KeyName = UserName ++ "-1",
    create_user(U#user_data{username = UserName, keys = [KeyName]});
create_user(U = #user_data{realname = none}) ->
    create_user(U#user_data{realname = ask_realname()});
create_user(U = #user_data{contact_info = none}) ->
    create_user(U#user_data{contact_info = [{"email", ask_email()}]});
create_user(U = #user_data{realm        = Realm,
                           username     = UserName,
                           realname     = RealName,
                           contact_info = [{"email", Email}]}) ->
    Instructions =
        "~nUSER DATA CONFIRMATION~n"
        "Please correct or confirm the user's data:~n"
        "[1] Realm    : ~ts~n"
        "[2] Username : ~ts~n"
        "[3] Real Name: ~ts~n"
        "[4] Email    : ~ts~n"
        "Press a number to select something to change, or [ENTER] to accept.~n",
    ok = io:format(Instructions, [Realm, UserName, RealName, Email]),
    case zx_tty:get_input() of
        "1" -> create_user(U#user_data{realm        = none});
        "2" -> create_user(U#user_data{username     = none});
        "3" -> create_user(U#user_data{realname     = none});
        "4" -> create_user(U#user_data{contact_info = none});
        ""  ->
            ok = store_user(U),
            UserName;
        _   ->
            ok = zx_tty:derp(),
            create_user(U)
    end.


-spec store_user(#user_data{}) -> ok.

store_user(#user_data{realm        = Realm,
                      username     = UserName,
                      realname     = RealName,
                      contact_info = ContactInfo,
                      keys         = KeyNames}) ->
    UserConf =
        [{realm,        Realm},
         {username,     UserName},
         {realname,     RealName},
         {contact_info, ContactInfo},
         {keys,         KeyNames}],
    ok = gen_keys(Realm, KeyNames),
    UserConfPath = filename:join(zx_lib:path(etc, Realm), UserName ++ ".user"),
    zx_lib:write_terms(UserConfPath, UserConf).


-spec create_userfile() -> ok.

create_userfile() ->
    case select_realm() of
        error -> {error, "No realms configured.", 61};
        Realm -> create_userfile(Realm)
    end.


create_userfile(Realm) ->
    UserName = select_user(Realm),
    create_userfile(Realm, UserName).


create_userfile(Realm, UserName) ->
    UserConf = filename:join(zx_lib:path(etc, Realm), UserName ++ ".user"),
    {ok, UserData} = file:consult(UserConf),
    Keys = proplists:get_value(keys, UserData),
    Load =
        fun(KeyName, Acc) ->
            case file:read_file(zx_key:path(public, {Realm, KeyName})) of
                {ok, Der} ->
                    SHA512 = crypto:hash(sha512, Der),
                    Public = {SHA512, Der},
                    [{KeyName, none, Public} | Acc];
                _ ->
                    Acc
            end
        end,
    PubKeyData = lists:foldl(Load, [], Keys),
    UserFile = Realm ++ "-" ++ UserName ++ ".zpuf",
    Bin = term_to_binary({UserData, PubKeyData}),
    ok = file:write_file(UserFile, Bin),
    Message =
        "Wrote Zomp public user file to ~ts.~n"
        "This file can be given to a sysop from ~ts and added to the realm.~n"
        "It ONLY contains PUBLIC KEY data.~n",
    io:format(Message, [UserFile, Realm]).


-spec export_user() -> ok.

export_user() ->
    case select_realm() of
        error -> {error, "No realms configured.", 61};
        Realm -> export_user(Realm)
    end.


export_user(Realm) ->
    UserName = select_user(Realm),
    UserConf = filename:join(zx_lib:path(etc, Realm), UserName ++ ".user"),
    {ok, UserData} = file:consult(UserConf),
    Keys = proplists:get_value(keys, UserData),
    Load =
        fun(KeyName, Acc) ->
            Key =
                case file:read_file(zx_key:path(private, {Realm, KeyName})) of
                    {ok, KDer} -> {crypto:hash(sha512, KDer), KDer};
                    _          -> none
                end,
            Pub =
                case file:read_file(zx_key:path(public, {Realm, KeyName})) of
                    {ok, PDer} -> {crypto:hash(sha512, PDer), PDer};
                    _          -> none
                end,
            [{KeyName, Key, Pub} | Acc]
        end,
    KeyData = lists:foldl(Load, [], Keys),
    UserFile = Realm ++ "-" ++ UserName ++ ".zduf",
    Bin = term_to_binary({UserData, KeyData}),
    ok = file:write_file(UserFile, Bin),
    Message =
        "Wrote Zomp DANGEROUS user file to ~ts.~n"
        "WARNING: This file contains your PRIVATE KEYS and should NEVER be shared with "
        "anyone. Its only use is for the `import user [.zduf]` command!~n",
    io:format(Message, [UserFile]).


-spec import_user(file:filename()) -> zx:outcome().

import_user(Path) ->
    case file:read_file(Path) of
        {ok, Bin}       -> import_user2(Bin);
        {error, enoent} -> {error, "Bad path/missing file.", 2};
        {error, eacces} -> {error, "Can't read file: bad permissions.", 13};
        {error, eisdir} -> {error, "The path provided is a directory.", 21};
        Error           -> Error
    end.


import_user2(Bin) ->
    case zx_lib:b_to_t(Bin) of
        {ok, {UserData, []}} ->
            ok = log(info, "Note: This user file does not have any keys."),
            import_user3(UserData, []);
        {ok, {UserData, KeyData}} ->
            import_user3(UserData, KeyData);
        error ->
            {error, "Bad data. Is this really a legitimate .zduf or .zpuf?", 1}
    end.


import_user3(UserData, KeyData) ->
    Realm = proplists:get_value(realm, UserData),
    UserName = proplists:get_value(username, UserData),
    case filelib:is_dir(zx_lib:path(etc, Realm)) of
        true ->
            UserConf = filename:join(zx_lib:path(etc, Realm), UserName ++ ".user"),
            ok = zx_lib:write_terms(UserConf, UserData),
            import_user4(Realm, UserName, KeyData);
        false ->
            {error, "User is from a realm which is not available locally.", 1}
    end.


import_user4(Realm, UserName, KeyData) ->
    Write =
        fun
            ({KeyName, {KeySHA512, KeyDer}, none}) ->
                KeySHA512 = crypto:hash(sha512, KeyDer),
                KeyID = {Realm, KeyName},
                file:write_file(zx_key:path(private, KeyID), KeyDer);
            ({KeyName, none, {PubSHA512, PubDer}}) ->
                PubSHA512 = crypto:hash(sha512, PubDer),
                KeyID = {Realm, KeyName},
                file:write_file(zx_key:path(public, KeyID), PubDer);
            ({KeyName, {KeySHA512, KeyDer}, {PubSHA512, PubDer}}) ->
                KeySHA512 = crypto:hash(sha512, KeyDer),
                PubSHA512 = crypto:hash(sha512, PubDer),
                KeyID = {Realm, KeyName},
                file:write_file(zx_key:path(private, KeyID), KeyDer),
                file:write_file(zx_key:path(public,  KeyID), PubDer)
        end,
    ok = lists:foreach(Write, KeyData),
    log(info, "Imported user ~ts to realm ~ts.", [UserName, Realm]).


-spec list_users(Realm) -> UserNames
    when Realm     :: zx:realm(),
         UserNames :: [zx:user_name()].

list_users(Realm) ->
    Pattern = filename:join(zx_lib:path(etc, Realm), "*.user"),
    [filename:basename(UN, ".user") || UN <- filelib:wildcard(Pattern)].


-spec gen_keys(Realm, KeyNames) -> ok
    when Realm    :: zx:realm(),
         KeyNames :: [zx:key_names()].

gen_keys(Realm, KeyNames) ->
    ok = io:format("Generating keys. This might take a while, so settle in...~n"),
    GenRSA = fun(KeyName) -> zx_key:generate_rsa({Realm, KeyName}) end,
    lists:foreach(GenRSA, KeyNames).


-spec pick_realm() -> Result
    when Result :: {ok, zx:realm()}
                 | {error, no_realms}.

pick_realm() ->
    case zx_lib:list_realms() of
        [] ->
            ok = log(warning, "No realms configured! Exiting..."),
            {error, no_realms};
        Realms ->
            ok = io:format("Select a realm:~n"),
            Options = lists:zip(Realms, Realms),
            Realm = zx_tty:select(Options),
            {ok, Realm}
    end.


-spec ask_project_name() -> string().

ask_project_name() ->
    Instructions =
        "~nPROJECT NAME~n"
        "Enter the natural, readable name of your project.~n"
        "(Any valid UTF-8 printables are legal.)~n",
    case zx_tty:get_input(Instructions) of
        "" ->
            ok = io:format("Please enter a project name."),
            ask_project_name();
        String ->
            unicode:characters_to_list(String, utf8)
    end.


-spec ask_username() -> zx:user_name().

ask_username() ->
    Instructions =
        "~nUSERNAME~n"
        "Enter a username.~n"
        "Names can contain only lower-case letters, numbers and the underscore.~n"
        "Names must begin with a lower-case letter.~n",
    zx_tty:get_lower0_9(Instructions).


-spec ask_realname() -> string().

ask_realname() ->
    Instructions =
        "~nREAL NAME~n"
        "Enter the user's real name (or whatever name people recognize).~n"
        "(Any valid UTF-8 printables are legal.)~n",
    String = zx_tty:get_input(Instructions, [], "[ENTER] to leave blank"),
    unicode:characters_to_list(String, utf8).


-spec ask_author() -> {string(), string()}.

ask_author() ->
    Instructions =
        "~nAUTHOR'S NAME~n"
        "Enter the authors's real name (or whatever name people recognize).~n"
        "(Any valid UTF-8 printables are legal.)~n",
    String = zx_tty:get_input(Instructions, [], "[ENTER] to leave blank"),
    Author = unicode:characters_to_list(String, utf8),
    Email = ask_email_optional(),
    {Author, Email}.


-spec ask_copyright() -> {string(), string()}.

ask_copyright() ->
    Instructions =
        "~nCOPYRIGHT HOLDER'S NAME~n"
        "Enter the copyright's real name. This may be a person, company, group, etc.~n"
        "There are no rules for this one. Any valid UTF-8 printables are legal.~n"
        "This can also be left blank.~n",
    String = zx_tty:get_input(Instructions, [], "[ENTER] to leave blank"),
    Holder = unicode:characters_to_list(String, utf8),
    Email = ask_email_optional(),
    {Holder, Email}.


-spec ask_license() -> string().

ask_license() ->
    Message =
        "~nLICENSE SELECTION~n"
        "Note that many more FOSS license identifiers are available at "
        "https://spdx.org/licenses/~n",
    ok = io:format(Message),
    Options =
        [{"Apache License 2.0",                             "Apache-2.0"},
         {"BSD 3-Clause with Attribution",                  "BSD-3-Clause-Attribution"},
         {"BSD 2-Clause / FreeBSD",                         "BSD-2-Clause-FreeBSD"},
         {"GNU General Public License (GPL) v3.0 only",     "GPL-3.0-only"},
         {"GNU General Public License (GPL) v3.0 or later", "GPL-3.0-or-later"},
         {"GNU Library (LGPL) v3.0 only",                   "LGPL-3.0-only"},
         {"GNU Library (LGPL) v3.0 or later",               "LGPL-3.0-or-later"},
         {"MIT license",                                    "MIT"},
         {"Mozilla Public License 2.0",                     "MPL-2.0"},
         {"Public Domain",                                  "Public-Domain"},
         {"[proprietary]",                                  proprietary},
         {"[skip and leave blank]",                         skip}],
    case zx_tty:select(Options) of
        skip ->
            "";
        proprietary ->
            Notice =
                "~nNOTE ON PROPRIETARY USE~n"
                "It is recommended that you check with your legal department or "
                "advisor to determine whether your proprietary project is compatible "
                "with the GPL version of Zomp/ZX.~n"
                "If not (mostly when deploying client-side code that links to ZX or "
                "other GPL/dual-license libraries), Zomp/ZX should be dual-licensed.~n",
            ok = io:format(Notice),
            "";
        License ->
            License
    end.



-spec ask_email() -> string().

ask_email() ->
    case zx_tty:get_input(email_instructions()) of
        "" ->
            ok = io:format("An email address is required.~n"),
            ask_email();
        String ->
            case check_email(String) of
                ok    -> String;
                error -> ask_email()
            end
    end.


-spec ask_email_optional() -> string().

ask_email_optional() ->
    case zx_tty:get_input(email_instructions(), [], "[ENTER] to leave blank") of
        "" ->
            "";
        String ->
            case check_email(String) of
                ok    -> String;
                error -> ask_email()
            end
    end.


-spec check_email(Address :: string()) -> ok | error.

check_email(Address) ->
    case string:lexemes(Address, "@") of
        [User, Host] ->
            case {zx_lib:valid_lower0_9(User), zx_lib:valid_label(Host)} of
                {true, true} ->
                    ok;
                {false, true} ->
                    Message = "The user part of the email address seems invalid.~n",
                    ok = io:format(Message),
                    error;
                {true, false} ->
                    Message = "The host part of the email address seems invalid.~n",
                    ok = io:format(Message),
                    error;
                {false, false} ->
                    Message = "This email address is totally bonkers. Try again.~n",
                    ok = io:format(Message),
                    error
            end;
        _ ->
            ok = io:format("Don't get fresh. Try again. For real this time.~n"),
            error
    end.


email_instructions() ->
    "~nEMAIL~n"
    "Enter an email address.~n".


-spec realm_exists(zx:realm()) -> boolean().
%% @private
%% Checks for remnants of a realm.

realm_exists(Realm) ->
    Managed = lists:member(Realm, zx_sys_conf:managed(zx_sys_conf:load())),
    Dirs = [zx_lib:path(D, Realm) || D <- [etc, var, tmp, log, key, zsp, lib]],
    Check = fun(D) -> filelib:is_file(D) end,
    Found = lists:any(Check, Dirs),
    Managed or Found.


-spec make_realm_dirs(zx:realm()) -> ok.
%% @private
%% This is an unsafe operation. The caller must be sure realm_exists/1 returns false
%% or be certain some other way that the file:make_sure/1 calls will succeed.

make_realm_dirs(Realm) ->
    Dirs = [zx_lib:path(D, Realm) || D <- [etc, var, tmp, log, key, zsp, lib]],
    Make = fun(D) -> ok = zx_lib:force_dir(D) end,
    lists:foreach(Make, Dirs).


%% FIXME
%-spec configure_zomp() -> ok.
%
%configure_zomp() ->
%    ZompSettings =
%        [{node,        16},
%         {vampire,     16},
%         {leaf,        256},
%         {listen_port, 11311},
%         {public_port, 11311}],
%    io:format("~tw~n", [ZompSettings]).


-spec create_realmfile() -> ok.

create_realmfile() ->
    Realm = select_realm(),
    create_realmfile(Realm).


-spec create_realmfile(zx:realm()) -> ok.

create_realmfile(Realm) ->
    {ok, RealmConf} = zx_lib:load_realm_conf(Realm),
    ok = log(info, "Realm found, creating realm file..."),
    KeyName = maps:get(key, RealmConf),
    PubKeyPath = zx_key:path(public, {Realm, KeyName}),
    {ok, PubDER} = file:read_file(PubKeyPath),
    Blob = term_to_binary({RealmConf, PubDER}),
    ZRF = Realm ++ ".zrf",
    ok = file:write_file(ZRF, Blob),
    log(info, "Realm conf file written to ~ts", [ZRF]).


-spec drop_realm(zx:realm()) -> ok.

drop_realm(Realm) ->
    case realm_exists(Realm) of
        true ->
            Message =
                "~nWARNING: Are you SURE you want to remove realm ~ts?~n"
                "(Only \"Y\" will confirm this action.)~n",
            ok = io:format(Message, [Realm]),
            case zx_tty:get_input() of
                "Y" ->
                    Dirs = [etc, var, tmp, log, key, zsp, lib],
                    RM = fun(D) -> ok = zx_lib:rm_rf(zx_lib:path(D, Realm)) end,
                    ok = lists:foreach(RM, Dirs),
                    ok = abdicate(Realm),
                    log(info, "All traces of realm ~ts have been removed.");
                _ ->
                    log(info, "Aborting.")
            end;
        false ->
            log(info, "Could not find any trace of realm ~ts", [Realm])
    end.


-spec takeover(zx:realm()) -> ok.
%% @private
%% Assume responsibilities as the prime node for the given realm. Only works if
%% the realm exists, of course.

takeover(Realm) ->
    SysConf = zx_sys_conf:load(),
    case zx_sys_conf:add_managed(Realm, SysConf) of
        {ok, NewConf}         -> zx_sys_conf:save(NewConf);
        {error, unconfigured} -> log(error, "Cannot take over an unconfigured realm.")
    end.


-spec abdicate(zx:realm()) -> ok.

abdicate(Realm) ->
    SysConf = zx_sys_conf:load(),
    case zx_sys_conf:rem_managed(Realm, SysConf) of
        {ok, NewConf}      -> zx_sys_conf:save(NewConf);
        {error, unmanaged} -> log(error, "Cannot abdicate an unmanaged realm.")
    end.


-spec set_timeout(string()) -> zx:outcome().

set_timeout(String) ->
    case string:to_integer(String) of
        {Value, ""} when Value > 0 ->
            zx_sys_conf:save(zx_sys_conf:timeout(Value, zx_sys_conf:load()));
        _ ->
            {error, "Enter a positive integer. Common values are 3, 5 and 10.", 22}
    end.


-spec add_mirror() -> ok.

add_mirror() ->
    ok = log(info, "Adding a mirror to the local configuration..."),
    SysConf = zx_sys_conf:load(),
    ok =
        case zx_sys_conf:mirrors(SysConf) of
            [] ->
                ok;
            Current ->
                ok = io:format("Current mirrors:~n"),
                Print =
                    fun({A, P}) ->
                        S = stringify_address(A),
                        io:format("* ~s:~w~n", [S, P])
                    end,
                lists:foreach(Print, Current)
        end,
    Host = ask_addr(),
    Port = ask_port(),
    zx_sys_conf:save(zx_sys_conf:add_mirror({Host, Port}, SysConf)).


-spec drop_mirror() -> ok.

drop_mirror() ->
    ok = log(info, "Removing mirrors from the local configuration..."),
    SysConf = zx_sys_conf:load(),
    zx_sys_conf:save(drop_mirror(SysConf)).


-spec drop_mirror(zx_sys_conf:data()) -> zx_sys_conf:data().

drop_mirror(SysConf) ->
    case zx_sys_conf:mirrors(SysConf) of
        [] ->
            ok = log(info, "No mirrors to drop!"),
            SysConf;
        Current ->
            ok = io:format("Pick a host to drop:~n"),
            Optionize =
                fun(Host = {A, P}) ->
                    Label = io_lib:format("~s:~w", [stringify_address(A), P]),
                    {Label, Host}
                end,
            Options = lists:map(Optionize, Current),
            Selection = zx_tty:select(Options),
            zx_sys_conf:rem_mirror(Selection, SysConf)
    end.


-spec select_realm() -> {ok, zx:realm()} | error.

select_realm() ->
    case zx_lib:list_realms() of
        []      -> error;
        [Realm] -> Realm;
        Realms  -> zx_tty:select_string(Realms)
    end.


-spec select_user(zx:realm()) -> zx:user_name().

select_user(Realm) ->
    case list_users(Realm) of
        [] ->
            Message =
                "A user record is required to complete this action. Creating now...",
            ok = log(info, Message),
            create_user(#user_data{realm = Realm});
        [UserName] ->
            UserName;
        UserNames ->
            zx_tty:select_string(UserNames)
    end.


-spec select_private_key(zx:user_id()) -> zx:key_name().

select_private_key(UserID = {Realm, UserName}) ->
    KeyDir = zx_lib:path(key, Realm),
    ok = zx_lib:force_dir(KeyDir),
    Pattern = filename:join(KeyDir, UserName ++ "-*.key.der"),
    case [filename:basename(F, ".key.der") || F <- filelib:wildcard(Pattern)] of
        []        -> zx_key:prompt_keygen(UserID);
        [KeyName] -> KeyName;
        KeyNames  -> zx_tty:select_string(KeyNames)
    end.
