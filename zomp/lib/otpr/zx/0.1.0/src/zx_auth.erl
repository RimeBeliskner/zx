%%% @doc
%%% ZX Auth
%%%
%%% This module is where all the AUTH type command code lives. AUTH commands are special
%%% because they do not involve the zx_daemon at all, though they do perform network
%%% operations.
%%%
%%% All AUTH procedures terminate the runtime once complete.
%%% @end

-module(zx_auth).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([list_pending/1, list_approved/1,
         submit/1, review/1, approve/1, reject/1, accept/1,
         list_users/1, list_packagers/1, list_maintainers/1, list_sysops/1,
         add_user/1,
         add_packager/2, rem_packager/2,
         add_maintainer/2, rem_maintainer/2,
         add_sysop/1,
         add_package/1]).

-include("zx_logger.hrl").



%%% Functions


-spec list_pending(PackageName :: string()) -> zx:outcome().
%% @private
%% List the versions of a package that are pending review. The package name is input by
%% the user as a string of the form "otpr-zomp" and the output is a list of full
%% package IDs, printed one per line to stdout (like "otpr-zomp-3.2.2").

list_pending(PackageName) ->
    case zx_lib:package_id(PackageName) of
        {ok, {Realm, Name, {z, z, z}}} -> list_pending2(Realm, Name);
        Error                          -> Error
    end.

list_pending2(Realm, Name) ->
    case connect(Realm) of
        {ok, Socket} -> list_pending3(Realm, Name, Socket);
        Error        -> Error
    end.

list_pending3(Realm, Name, Socket) ->
    Message = <<"ZOMP AUTH 1:", 0:24, 5:8, (term_to_binary({Realm, Name}))/binary>>,
    ok = gen_tcp:send(Socket, Message),
    receive
        {tcp, Socket, <<0:8, Bin/binary>>} -> list_pending4(Realm, Name, Socket, Bin);
        {tcp, Socket, Error}               -> done(Socket, Error);
        {tcp_closed, Socket}               -> {error, "Socket closed unexpectedly."}
        after 5000                         -> done(Socket, timeout)
    end.

list_pending4(Realm, Name, Socket, Bin) ->
    ok = zx_net:disconnect(Socket),
    case zx_lib:b_to_ts(Bin) of
        {ok, Versions} -> list_pending5(Realm, Name, Versions);
        Error          -> Error
    end.

list_pending5(Realm, Name, Versions) ->
    Print =
        fun(Version) ->
            PackageID = {Realm, Name, Version},
            {ok, PackageString} = zx_lib:package_string(PackageID),
            io:format("~ts~n", [PackageString])
        end,
    lists:foreach(Print, Versions).


-spec list_approved(zx:realm()) -> zx:outcome().
%% @private
%% List the package ids of all packages waiting in the resign queue for the given realm,
%% printed to stdout one per line.

list_approved(Realm) ->
    case connect(Realm) of
        {ok, Socket} -> list_approved2(Realm, Socket);
        Error        -> Error
    end.

list_approved2(Realm, Socket) ->
    Message = <<"ZOMP AUTH 1:", 0:24, 7:8, (term_to_binary(Realm))/binary>>,
    ok = gen_tcp:send(Socket, Message),
    receive
        {tcp, Socket, <<0:8, Bin/binary>>} -> list_approved3(Socket, Bin, Realm);
        {tcp, Socket, Error}               -> done(Socket, Error);
        {tcp_closed, Socket}               -> {error, "Socket closed unexpectedly."}
        after 5000                         -> done(Socket, timeout)
    end.

list_approved3(Socket, Bin, Realm) ->
    ok = zx_net:disconnect(Socket),
    case zx_lib:b_to_ts(Bin) of
        {ok, PackageIDs} -> list_approved4(Realm, PackageIDs);
        Error            -> Error
    end.

list_approved4(Realm, PackageIDs) ->
    Print =
        fun({Name, Version}) ->
            {ok, PackageString} = zx_lib:package_string({Realm, Name, Version}),
            io:format("~ts~n", [PackageString])
        end,
    lists:foreach(Print, PackageIDs). 


-spec submit(ZspPath :: file:filename()) -> zx:outcome().
%% @private
%% Submit a package to the appropriate "prime" server for the given realm.

submit(ZspPath) ->
    case file:read_file(ZspPath) of
        {ok, ZspBin} -> submit2(ZspBin);
        Error        -> Error
    end.

submit2(ZspBin = <<SigSize:24, Sig:SigSize/binary, Signed/binary>>) ->
    <<MetaSize:16, MetaBin:MetaSize/binary, _/binary>> = Signed,
    {ok, {PackageID = {Realm, _, _}, KeyName, _, _}} = zx_lib:b_to_ts(MetaBin),
    UserName = zx_local:select_user(Realm),
    KeyID = {Realm, KeyName},
    case zx_key:ensure_keypair(KeyID) of
        true ->
            {ok, PKey} = zx_key:load(public,  KeyID),
            {ok, DKey} = zx_key:load(private, KeyID),
            case zx_key:verify(Signed, Sig, PKey) of
                true  -> submit3(PackageID, ZspBin, UserName, KeyName, DKey);
                false -> {error, bad_sig}
            end;
        Error ->
            Error
    end.

submit3({Realm, Name, Ver}, ZspBin, UserName, KeyName, Key) ->
    Payload = {Realm, UserName, KeyName, {Name, Ver}},
    case connect(Realm) of
        {ok, Socket} -> submit4(Socket, Payload, ZspBin, Key);
        Error        -> Error
    end.

submit4(Socket, Payload, ZspBin, Key) ->
    Command = 1,
    Request = pack_and_sign(Command, Payload, Key),
    ok = gen_tcp:send(Socket, <<"ZOMP AUTH 1:", Request/binary>>),
    case zx_net:tx(Socket, ZspBin) of
        ok    -> done(Socket);
        Error -> done(Socket, Error)
    end.


-spec review(PackageString :: string()) -> zx:outcome().

review(PackageString) ->
    case zx_lib:package_id(PackageString) of
        {ok, PackageID} -> review2(PackageID);
        Error           -> Error
    end.

review2(PackageID = {Realm, _, _}) ->
    case connect(Realm) of
        {ok, Socket} -> review3(PackageID, Socket);
        Error        -> Error
    end.

review3(PackageID, Socket) ->
    Command = 8,
    TermBin = term_to_binary(PackageID),
    Request = <<0:24, Command:8, TermBin/binary>>,
    ok = gen_tcp:send(Socket, <<"ZOMP AUTH 1:", Request/binary>>),
    receive
        {tcp, Socket, <<0:1, 0:7>>} -> review4(PackageID, Socket);
        {tcp, Socket, Bin}          -> done(Socket, Bin);
        {tcp_closed, Socket}        -> {error, tcp_closed}
        after 5000                  -> done(Socket, timeout)
    end.

review4(PackageID, Socket) ->
    case zx_net:rx(Socket) of
        {ok, <<Size:24, Sig:Size/binary, Signed/binary>>} ->
            ok = zx_net:disconnect(Socket),
            review5(PackageID, Sig, Signed);
        Error ->
            done(Socket, Error)
    end.

review5(PackageID, Sig, Signed = <<Size:16, MetaBin:Size/binary, TgzBin/binary>>) ->
    case zx_lib:b_to_ts(MetaBin) of
        {ok, {PackageID = {Realm, _, _}, KeyName, _, _}} ->
            review6(PackageID, {Realm, KeyName}, Signed, Sig, TgzBin);
        {ok, {UnexpectedID, _, _, _}} ->
            {ok, Requested} = zx_lib:package_string(PackageID),
            {ok, Unexpected} = zx_lib:package_string(UnexpectedID),
            Message = "Requested ~ts, but inside was ~ts! Aborting.",
            ok = log(warning, Message, [Requested, Unexpected]),
            {error, "Wrong package received.", 29};
        error ->
            {error, bad_response}
    end.

review6(PackageID, KeyID, Signed, Sig, TgzBin) ->
    case zx_key:load(public, KeyID) of
        {ok, Key} -> review7(PackageID, Key, Signed, Sig, TgzBin);
        Error     -> Error % TODO: Fetch unknown keys
    end.


review7(PackageID, SigKey, Signed, Sig, TgzBin) ->
    case zx_key:verify(Signed, Sig, SigKey) of
        true  -> review8(PackageID, TgzBin);
        false -> {error, bad_sig}
    end.

review8(PackageID, TgzBin) ->
    {ok, PackageString} = zx_lib:package_string(PackageID),
    case file:make_dir(PackageString) of
        ok ->
            review9(PackageString, TgzBin);
        {error, Error} ->
            Message = "Creating dir ./~ts failed with ~ts. Aborting.",
            ok = log(error, Message, [PackageString, Error]),
            {error, Error}
    end.

review9(PackageString, TgzBin) ->
    ok = erl_tar:extract({binary, TgzBin}, [compressed, {cwd, PackageString}]),
    log(info, "Sources unpacked to ./~ts", [PackageString]).


approve(Package) -> package_operation(9, Package).

reject(Package)  -> package_operation(10, Package).


-spec package_operation(Code :: 9 | 10, Package :: string()) -> zx:outcome().

package_operation(Code, Package) ->
    case zx_lib:package_id(Package) of
        {ok, {Realm, Name, Version}} -> make_su_request(Code, Realm, {Name, Version});
        Error                        -> Error
    end.


-spec accept(PackageString :: string()) -> zx:outcome().

accept(PackageString) ->
    case zx_lib:package_id(PackageString) of
        {ok, PackageID} -> accept2(PackageID);
        Error           -> Error
    end.

accept2(PackageID = {Realm, Name, Version}) ->
    case connect_auth(Realm) of
        {ok, AuthConn} -> accept3(PackageID, {Name, Version}, AuthConn);
        Error          -> Error
    end.

accept3(PackageID, PV, {UserName, KeyName, Key, Tag, Socket}) ->
    Command = 11,
    Payload = {Tag, UserName, KeyName, PV},
    Request = pack_and_sign(Command, Payload, Key),
    ok = gen_tcp:send(Socket, <<0:8, Request/binary>>),
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<0:1, 0:7>>} -> accept4(PackageID, KeyName, Key, Socket);
        {tcp, Socket, Bin}          -> done(Socket, Bin);
        {tcp_closed, Socket}        -> {error, tcp_closed}
        after 5000                  -> done(Socket, timeout)
    end.

accept4(PackageID, KeyName, Key, Socket) ->
    case zx_net:rx(Socket) of
        {ok, <<Size:24, Sig:Size/binary, Signed/binary>>} ->
            accept5(PackageID, {KeyName, Key, Socket}, Sig, Signed);
        Error ->
            done(Socket, Error)
    end.

accept5(PackageID,
        Auth,
        Sig,
        Signed = <<Size:16, MetaBin:Size/binary, TgzBin/binary>>) ->
    case zx_lib:b_to_ts(MetaBin) of
        {ok, Meta = {PackageID = {Realm, _, _}, SigKeyName, _, _}} ->
            accept6(Meta, Auth, {Realm, SigKeyName}, Signed, Sig, TgzBin);
        {ok, {UnexpectedID, _, _, _}} ->
            {ok, Requested} = zx_lib:package_string(PackageID),
            {ok, Unexpected} = zx_lib:package_string(UnexpectedID),
            Message = "Requested ~ts, but inside was ~ts! Aborting.",
            ok = log(warning, Message, [Requested, Unexpected]),
            {error, "Wrong package received.", 29};
        error ->
            {error, bad_response}
    end.
    
accept6(Meta, Auth, SigKeyID, Signed, Sig, TgzBin) ->
    case zx_key:load(public, SigKeyID) of
        {ok, SigKey} -> accept7(Meta, Auth, SigKey, Signed, Sig, TgzBin);
        Error        -> Error % TODO: Fetch unknown keys
    end.

accept7(Meta, Auth, SigKey, Signed, Sig, TgzBin) ->
    case zx_key:verify(Signed, Sig, SigKey) of
        true  -> accept8(Meta, Auth, TgzBin);
        false -> {error, bad_sig}
    end.

accept8({PackageID, _, Deps, Modules}, {KeyName, Key, Socket}, TgzBin) ->
    MetaBin = term_to_binary({PackageID, KeyName, Deps, Modules}),
    MetaSize = byte_size(MetaBin),
    SignMe = <<MetaSize:16, MetaBin:MetaSize/binary, TgzBin/binary>>,
    Sig = public_key:sign(SignMe, sha512, Key),
    SigSize = byte_size(Sig),
    ZspData = <<SigSize:24, Sig:SigSize/binary, SignMe/binary>>,
    ok = zx_net:tx(Socket, ZspData),
    done(Socket).


list_users(Realm) ->
    list_users(2, Realm).

list_packagers(Package) ->
    {ok, {Realm, Name, {z, z, z}}} = zx_lib:package_id(Package),
    list_users(3, {Realm, Name}).

list_maintainers(Package) ->
    {ok, {Realm, Name, {z, z, z}}} = zx_lib:package_id(Package),
    list_users(4, {Realm, Name}).

list_sysops(Realm) ->
    list_users(5, Realm).


-spec list_users(Command, Target) -> zx:outcome()
    when Command :: 2..5,
         Target  :: zx:realm() | zx:package().

list_users(Command, Target) ->
    case make_uu_request(Command, Target) of
        {ok, Users} -> lists:foreach(fun print_user/1, Users);
        Error       -> Error
    end.

print_user({UserName, RealName, [{"email", Email}]}) ->
    io:format("~ts (~ts <~ts>) ~n", [UserName, RealName, Email]).


-spec add_user(file:filename()) -> zx:outcome().

add_user(ZPUF) ->
    case file:read_file(ZPUF) of
        {ok, Bin} -> add_user2(Bin);
        Error     -> Error
    end.

add_user2(Bin) ->
    case zx_lib:b_to_t(Bin) of
        {ok, {UserInfo, KeyData}} ->
            Realm = proplists:get_value(realm, UserInfo),
            UserData = {proplists:get_value(username, UserInfo),
                        proplists:get_value(realname, UserInfo),
                        proplists:get_value(contact_info, UserInfo),
                        [setelement(2, KD, none) || KD <- KeyData]},
            Command = 13,
            make_su_request(Command, Realm, UserData);
        error ->
            {error, "Bad user file.", 1}
    end.


add_packager(Package, User)   -> user_auth_operation(15, Package, User).

rem_packager(Package, User)   -> user_auth_operation(16, Package, User).

add_maintainer(Package, User) -> user_auth_operation(17, Package, User).

rem_maintainer(Package, User) -> user_auth_operation(18, Package, User).


-spec user_auth_operation(Code, Package, User)-> zx:outcome()
    when Code    :: 15..18,
         Package :: string(),
         User    :: zx:user_name().

user_auth_operation(Code, Package, User) ->
    case zx_lib:package_id(Package) of
        {ok, {Realm, Name, {z, z, z}}} -> make_su_request(Code, Realm, {Name, User});
        Error                          -> Error
    end.


-spec add_sysop(file:filename()) -> zx:outcome().

add_sysop(UserFile) ->
    ok = log(info, "Would add ~ts to sysop list.", [UserFile]),
    {error, "Not yet implemented", 1}.


-spec add_package(zx:package()) -> zx:outcome().

add_package(PackageName) ->
    ok = file:set_cwd(zx_lib:zomp_dir()),
    case zx_lib:package_id(PackageName) of
        {ok, {Realm, Name, {z, z, z}}} -> add_package2(Realm, Name);
        Error                          -> Error
    end.

add_package2(Realm, Name) ->
    case connect_auth(Realm) of
        {ok, AuthConn} -> add_package3(Realm, Name, AuthConn);
        Error          -> Error
    end.

add_package3(Realm, Name, {UserName, KeyName, Key, Tag, Socket}) ->
    Command = 12,
    Package = {Realm, Name},
    Payload = {Tag, UserName, KeyName, Package},
    Request = pack_and_sign(Command, Payload, Key),
    ok = gen_tcp:send(Socket, <<0:8, Request/binary>>),
    done(Socket).



%%% Generic Request Forms

-spec make_uu_request(Command, Target) -> zx:outcome()
    when Command :: pos_integer(),
         Target  :: zx:realm() | zx:package() | zx:package_id().

make_uu_request(Command, Target) when is_tuple(Target) ->
    make_uu_request2(Command, element(1, Target), Target);
make_uu_request(Command, Target) when is_list(Target) ->
    make_uu_request2(Command, Target, Target).

make_uu_request2(Command, Realm, Target) ->
    case connect(Realm) of
        {ok, Socket} -> make_uu_request3(Command, Target, Socket);
        Error        -> Error
    end.

make_uu_request3(Command, Target, Socket) ->
    TermBin = term_to_binary(Target),
    Request = <<"ZOMP AUTH 1:", 0:24, Command:8, TermBin/binary>>,
    ok = gen_tcp:send(Socket, Request),
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<0:1, 0:7, Bin/binary>>} -> make_uu_request4(Bin);
        {tcp, Socket, Bin}                      -> done(Socket, Bin);
        {tcp_closed, Socket}                    -> {error, tcp_closed}
        after 5000                              -> done(Socket, timeout)
    end.

make_uu_request4(Bin) ->
    case zx_lib:b_to_ts(Bin) of
        error -> {error, bad_response};
        Term  -> Term
    end.


-spec make_su_request(Command, Realm, Data) -> zx:outcome()
    when Command :: 1 | 9 | 10 | 13..20,
         Realm   :: zx:realm(),
         Data    :: term().

make_su_request(Command, Realm, Data) ->
    AuthData = prep_auth(Realm),
    case connect(Realm) of
        {ok, Socket} -> make_su_request2(Command, Realm, Data, AuthData, Socket);
        Error        -> Error
    end.

make_su_request2(Command, Realm, Data, {Signatory, KeyName, Key}, Socket) ->
    Payload = {Realm, Signatory, KeyName, Data},
    Request = pack_and_sign(Command, Payload, Key),
    ok = gen_tcp:send(Socket, <<"ZOMP AUTH 1:", Request/binary>>),
    done(Socket).



%%% Connectiness with prime

-spec connect_auth(Realm) -> Result
    when Realm    :: zx:realm(),
         Result   :: {ok, AuthConn}
                   | {error, Reason},
         AuthConn :: {UserName   :: zx:user_name(),
                      KeyName    :: zx:key_name(),
                      Key        :: term(),
                      SSTag      :: zx:ss_tag(),
                      Socket     :: gen_tcp:socket()},
         Reason   :: term().
%% @private
%% Connect to one of the servers in the realm constellation.

connect_auth(Realm) ->
    UserData = prep_auth(Realm),
    case zx_lib:load_realm_conf(Realm) of
        {ok, RealmConf} ->
            connect_auth2(Realm, RealmConf, UserData);
        Error ->
            ok = log(error, "Realm ~160tp is not configured.", [Realm]),
            Error
    end.

connect_auth2(Realm, RealmConf, UserData) ->
    {Host, Port} = maps:get(prime, RealmConf),
    Options = [{packet, 4}, {mode, binary}, {active, once}],
    case gen_tcp:connect(Host, Port, Options, 5000) of
        {ok, Socket} ->
            connect_auth3(Socket, Realm, UserData);
        Error = {error, E} ->
            ok = log(warning, "Connection problem: ~160tp", [E]),
            {error, Error}
    end.

connect_auth3(Socket, Realm, UD = {UserName, KeyName, Key}) ->
    Null = 0,
    Timestamp = calendar:universal_time(),
    Payload = {Realm, Timestamp, UserName, KeyName},
    NullRequest = pack_and_sign(Null, Payload, Key),
    ok = gen_tcp:send(Socket, <<"ZOMP AUTH 1:", NullRequest/binary>>),
    receive
        {tcp, Socket, <<0:8, Bin/binary>>} -> connect_auth4(Socket, UD, Bin);
        {tcp, Socket, Bin}                 -> done(Socket, Bin);
        {tcp_closed, Socket}               -> {error, tcp_closed}
        after 5000                         -> done(Socket, timeout)
    end.

connect_auth4(Socket, {UserName, KeyName, Key}, Bin) ->
    case zx_lib:b_to_ts(Bin) of
        {ok, Tag} -> {ok, {UserName, KeyName, Key, Tag, Socket}};
        error     -> done(Socket, bad_response)
    end.


connect(Realm) ->
    case zx_lib:load_realm_conf(Realm) of
        {ok, RealmConf} ->
            {Host, Port} = maps:get(prime, RealmConf),
            Options = [{packet, 4}, {mode, binary}, {nodelay, true}, {active, once}],
            gen_tcp:connect(Host, Port, Options, 5000);
        Error ->
            ok = log(error, "Realm ~160tp is not configured.", [Realm]),
            Error
    end.


-spec prep_auth(Realm) -> {User, KeyName, Key}
    when Realm   :: zx:realm(),
         User    :: zx:user_id(),
         KeyName :: zx:key_id(),
         Key     :: term().
%% @private
%% Loads the appropriate User, KeyID and reads in a registered key for use in
%% connect_auth/4.

prep_auth(Realm) ->
    UserName = zx_local:select_user(Realm),
    KeyName = zx_local:select_private_key({Realm, UserName}),
    {ok, Key} = zx_key:load(private, {Realm, KeyName}),
    {UserName, KeyName, Key}.


pack_and_sign(Command, Payload, Key) ->
    Bin = term_to_binary(Payload),
    Signed = <<Command:8, Bin/binary>>,
    Sig = public_key:sign(Signed, sha512, Key),
    SSize = byte_size(Sig),
    <<SSize:24, Sig/binary, Signed/binary>>.


done(Socket) ->
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<0:1, 0:7>>} ->
            zx_net:disconnect(Socket);
        {tcp, Socket, Bin} ->
            ok = zx_net:disconnect(Socket),
            {error, zx_net:err_in(Bin)};
        {tcp_closed, Socket} ->
            {error, tcp_closed}
        after 5000 ->
            done(Socket, timeout)
    end.


done(Socket, Reason) ->
    ok = zx_net:disconnect(Socket),
    case is_binary(Reason) of
        true  -> {error, zx_net:err_in(Reason)};
        false -> {error, Reason}
    end.
