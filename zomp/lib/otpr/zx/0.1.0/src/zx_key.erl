%%% @doc
%%% ZX Key
%%%
%%% Abstraction module for dealing with keys.
%%%
%%%    "Ewwwww! Keys!"
%%%          -- Bertrand Russel
%%% @end

-module(zx_key).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([ensure_keypair/1, have_key/2, path/2,
         prompt_keygen/1, generate_rsa/1,
         load/2, verify/3]).

-include("zx_logger.hrl").


%%% Functions

-spec ensure_keypair(zx:key_id()) -> zx:outcome().
%% @private
%% Check if both the public and private key based on KeyID exists.

ensure_keypair(KeyID = {Realm, KeyName}) ->
    case {have_key(public, KeyID), have_key(private, KeyID)} of
        {true, true} ->
            true;
        {false, true} ->
            Format = "Public key ~ts/~ts cannot be found",
            Message = io_lib:format(Format, [Realm, KeyName]),
            {error, Message, 2};
        {true, false} ->
            Format = "Private key ~ts/~ts cannot be found",
            Message = io_lib:format(Format, [Realm, KeyName]),
            {error, Message, 2};
        {false, false} ->
            Format = "Key pair ~ts/~ts cannot be found",
            Message = io_lib:format(Format, [Realm, KeyName]),
            {error, Message, 2}
    end.


-spec have_key(Type, KeyID) -> boolean()
    when Type  :: public | private,
         KeyID :: zx:key_id().
%% @private
%% Determine whether the indicated key is present.

have_key(Type, KeyID) ->
    filelib:is_regular(path(Type, KeyID)).


-spec path(Type, KeyID) -> Path
    when Type  :: public | private,
         KeyID :: zx:key_id(),
         Path  :: file:filename().
%% @private
%% Given KeyID, return the path to the key type indicated.

path(public, {Realm, KeyName}) ->
    filename:join(zx_lib:path(key, Realm), KeyName ++ ".pub.der");
path(private, {Realm, KeyName}) ->
    filename:join(zx_lib:path(key, Realm), KeyName ++ ".key.der").



%%% Key generation

-spec prompt_keygen(zx:user_id()) -> zx:key_name().
%% @private
%% Prompt the user for a valid KeyPrefix to use for naming a new RSA keypair.

prompt_keygen(UserID = {Realm, UserName}) ->
    Message =
        "~nKEY NAME~n"
        "Enter a name for your new key pair.~n"
        "Valid names must start with a lower-case letter, and can include "
        "only lower-case letters, numbers, and underscores, but no series of "
        "consecutive underscores. (That is: [a-z0-9_])~n"
        "  Example: my_key~n",
    ok = io:format(Message),
    KeyTag = zx_tty:get_input(),
    case zx_lib:valid_lower0_9(KeyTag) of
        true ->
            KeyName = UserName ++ "-" ++ KeyTag,
            ok = zx_key:generate_rsa({Realm, KeyName}),
            KeyName;
        false ->
            ok = io:format("Bad key name ~ts. Try again.~n", [KeyTag]),
            prompt_keygen(UserID)
    end.


-spec generate_rsa(KeyID) -> Result
    when KeyID  :: zx:key_id(),
         Result :: ok
                 | {error, Reason},
         Reason :: keygen_fail
                 | exists.
%% @private
%% Generate an RSA keypair and write them in der format to the current directory, using
%% filenames derived from Prefix.
%% NOTE: The current version of this command is likely to only work on a unix system.

generate_rsa(KeyID = {Realm, KeyName}) ->
    BaseName = filename:join(zx_lib:path(key, Realm), KeyName),
    Pattern = BaseName ++ ".*.der",
    case filelib:wildcard(Pattern) of
        [] -> generate_rsa(KeyID, BaseName);
        _  -> {error, exists}
    end.


generate_rsa(KeyID, BaseName) ->
    PemFile = BaseName ++ ".pub.pem",
    KeyFile = path(private, KeyID),
    PubFile = path(public, KeyID),
    ok = log(info, "Generating ~s and ~s. Please be patient...", [KeyFile, PubFile]),
    case gen_p_key(KeyFile) of
        ok ->
            ok = der_to_pem(KeyFile, PemFile),
            {ok, PemBin} = file:read_file(PemFile),
            [PemData] = public_key:pem_decode(PemBin),
            Pub = public_key:pem_entry_decode(PemData),
            PubDer = public_key:der_encode('RSAPublicKey', Pub),
            ok = file:write_file(PubFile, PubDer),
            case check_key(KeyFile, PubFile) of
                true ->
                    ok = file:delete(PemFile),
                    log(info, "~ts and ~ts agree", [KeyFile, PubFile]);
                false ->
                    ok = lists:foreach(fun file:delete/1, [PemFile, KeyFile, PubFile]),
                    ok = log(error, "Something has gone wrong."),
                    {error, keygen_fail}
            end;
        {error, no_ssl} ->
            ok = log(error, "OpenSSL not found."),
            {error, keygen_fail}
    end.


-spec gen_p_key(KeyFile) -> Result
    when KeyFile :: file:filename(),
         Result  :: ok
                  | {error, no_ssl}.
%% @private
%% Format an openssl shell command that will generate proper 16k RSA keys.

gen_p_key(KeyFile) ->
    case openssl() of
        {ok, OpenSSL} ->
            Command =
                io_lib:format("~ts genpkey"
                              "    -algorithm rsa"
                              "    -out ~ts"
                              "    -outform DER"
                              "    -pkeyopt rsa_keygen_bits:16384",
                              [OpenSSL, KeyFile]),
            Out = os:cmd(Command),
            io:format(Out);
        Error ->
            Error
    end.


-spec der_to_pem(KeyFile, PemFile) -> ok
    when KeyFile :: file:filename(),
         PemFile :: file:filename().
%% @private
%% Format an openssl shell command that will convert the given keyfile to a pemfile.
%% The reason for this conversion is to sidestep some formatting weirdness that OpenSSL
%% injects into its generated DER formatted key output (namely, a few empty headers)
%% which Erlang's ASN.1 defintion files do not take into account. A conversion to PEM
%% then a conversion back to DER (via Erlang's ASN.1 module) resolves this in a reliable
%% way.

der_to_pem(KeyFile, PemFile) ->
    case openssl() of
        {ok, OpenSSL} ->
            Command =
                io_lib:format("~ts rsa"
                              "    -inform DER"
                              "    -in ~ts"
                              "    -outform PEM"
                              "    -pubout"
                              "    -out ~ts",
                              [OpenSSL, KeyFile, PemFile]),
            Out = os:cmd(Command),
            io:format(Out);
        Error ->
            Error
    end.


-spec check_key(KeyFile, PubFile) -> Result
    when KeyFile :: file:filename(),
         PubFile :: file:filename(),
         Result  :: true | false.
%% @private
%% Compare two keys for pairedness.

check_key(KeyFile, PubFile) ->
    {ok, KeyBin} = file:read_file(KeyFile),
    {ok, PubBin} = file:read_file(PubFile),
    Key = public_key:der_decode('RSAPrivateKey', KeyBin),
    Pub = public_key:der_decode('RSAPublicKey', PubBin),
    TestMessage = <<"Some test data to sign.">>,
    Signature = public_key:sign(TestMessage, sha512, Key),
    public_key:verify(TestMessage, sha512, Signature, Pub).


-spec openssl() -> Result
    when Result     :: {ok, Executable}
                     | {error, no_ssl},
         Executable :: file:filename().
%% @private
%% Attempt to locate the installed openssl executable for use in shell commands.
%% TODO: Determine whether it is even worth it to perform this check VS restricting
%%       os:cmd/1 directed zx_key functions by platform.

openssl() ->
    OpenSSL =
        case os:type() of
            {unix, _}  -> "openssl";
            {win32, _} -> "openssl.exe"
        end,
    case os:find_executable(OpenSSL) of
        false ->
            ok = log(error, "OpenSSL could not be found in this system's PATH."),
            ok = log(error, "Install OpenSSL and then retry."),
            {error, no_ssl};
        Path ->
            log(info, "OpenSSL executable found at: ~ts", [Path]),
            {ok, OpenSSL}
    end.


-spec load(Type, KeyID) -> Result
    when Type   :: private | public,
         KeyID  :: zx:key_id(),
         Result :: {ok, DecodedKey :: term()}
                 | {error, Reason :: file:posix()}.
%% @private
%% Hide the details behind reading and loading DER encoded RSA key files.

load(Type, KeyID) ->
    DerType =
        case Type of
            private -> 'RSAPrivateKey';
            public  -> 'RSAPublicKey'
        end,
    Path = path(Type, KeyID),
    case file:read_file(Path) of
        {ok, Bin} -> {ok, public_key:der_decode(DerType, Bin)};
        Error     -> Error
    end.


-spec verify(Data, Signature, PubKey) -> boolean()
    when Data      :: binary(),
         Signature :: binary(),
         PubKey    :: public_key:rsa_public_key().
%% @private
%% Curry out the choice of algorithm. This will probably disappear in a few more
%% versions as the details of sha512 and RSA gradually give way to the Brave New World.

verify(Data, Signature, PubKey) ->
    public_key:verify(Data, sha512, Signature, PubKey).
