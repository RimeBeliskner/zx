%%% @doc
%%% ZX: A suite of tools for Erlang development and deployment.
%%%
%%% ZX can:
%%%   - Create project templates for applications, libraries and escripts.
%%%   - Initialize existing projects for packaging and management.
%%%   - Create and manage zomp realms, users, keys, etc.
%%%   - Manage dependencies hosted in any zomp realm.
%%%   - Package, submit, pull-for-review, and resign-to-accept packages.
%%%   - Update, upgrade, and run any application from source that zomp tracks.
%%%   - Locally install packages from files and locally stored public keys.
%%%   - Build and run a local project from source using zomp dependencies.
%%%   - Start an anonymous zomp distribution node.
%%%   - Act as a unified code launcher for any projects (Erlang + ZX = deployed).
%%%
%%% ZX is currently limited in one specific way:
%%%   - Can only launch pure Erlang code.
%%%
%%% In the works:
%%%   - Support for LFE
%%%   - Support for Rust (cross-platform)
%%%   - Support for Elixir (as a peer language)
%%%   - Unified Windows installer to deploy Erlang, Rust, LFE, Elixir and ZX
%%% @end

-module(zx).
-behavior(application).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").


-export([do/0, do/1]).
-export([subscribe/1, unsubscribe/0]).
-export([start/2, stop/1, stop/0]).
-export([usage_exit/1]).

-export_type([serial/0, package_id/0, package/0, realm/0, name/0, version/0,
              identifier/0,
              host/0,
              key_id/0, key_name/0,
              user_id/0, user_name/0, contact_info/0, user_data/0,
              lower0_9/0, label/0,
              package_meta/0, ss_tag/0,
              outcome/0]).

-include("zx_logger.hrl").



%%% Type Definitions

-type serial()          :: integer().
-type package_id()      :: {realm(), name(), version()}.
-type package()         :: {realm(), name()}.
-type realm()           :: lower0_9().
-type name()            :: lower0_9().
-type version()         :: {Major :: non_neg_integer() | z,
                            Minor :: non_neg_integer() | z,
                            Patch :: non_neg_integer() | z}.
-type host()            :: {string() | inet:ip_address(), inet:port_number()}.
-type key_data()        :: {Name    :: key_name(),
                            Public  :: none | {SHA512 :: binary(), DER :: binary()},
                            Private :: none | {SHA512 :: binary(), DER :: binary()}}.
-type key_id()          :: {realm(), key_name()}.
-type key_name()        :: lower0_9().
-type user_data()       :: {ID       :: user_id(),
                            RealName :: string(),
                            Contact  :: [contact_info()],
                            Keys     :: [key_data()]}.
-type user_id()         :: {realm(), user_name()}.
-type user_name()       :: label().
-type contact_info()    :: {Type :: string(), Data :: string()}.
-type lower0_9()        :: [$a..$z | $0..$9 | $_].
-type label()           :: [$a..$z | $0..$9 | $_ | $- | $.].
-type package_meta()    :: #{package_id := package_id(),
                             deps       := [package_id()],
                             type       := app | lib}.
-type ss_tag()          :: {serial(), calendar:timestamp()}.

-type outcome()         :: ok
                         | {error, Reason :: term()}
                         | {error, Code :: non_neg_integer()}
                         | {error, Info :: string(), Code :: non_neg_integer()}.



%%% Command Dispatch

-spec do() -> no_return().

do() ->
    ok = io:setopts([{encoding, unicode}]),
    do([]).


-spec do(Args) -> no_return()
    when Args :: [string()].
%% Dispatch work functions based on the nature of the input arguments.

do(["help"]) ->
    done(help(top));
do(["help", "user"]) ->
    done(help(user));
do(["help", "dev"]) ->
    done(help(dev));
do(["help", "sysop"]) ->
    done(help(sysop));
do(["run", PackageString | ArgV]) ->
    ok = start(),
    not_done(run(PackageString, ArgV));
do(["list", "realms"]) ->
    done(zx_local:list_realms());
do(["list", "packages", Realm]) ->
    ok = start(),
    done(zx_local:list_packages(Realm));
do(["list", "versions", PackageName]) ->
    ok = start(),
    done(zx_local:list_versions(PackageName));
do(["latest", PackageString]) ->
    ok = start(),
    done(zx_local:latest(PackageString));
do(["import", "realm", RealmFile]) ->
    done(zx_local:import_realm(RealmFile));
do(["drop", "realm", Realm]) ->
    done(zx_local:drop_realm(Realm));
do(["logpath", Package, Run]) ->
    done(zx_local:logpath(Package, Run));
do(["status"]) ->
    done(zx_local:status());
do(["set", "timeout", String]) ->
    done(zx_local:set_timeout(String));
do(["add", "mirror"]) ->
    done(zx_local:add_mirror());
do(["drop", "mirror"]) ->
    done(zx_local:drop_mirror());
do(["create", "project"]) ->
    done(zx_local:create_project());
do(["runlocal" | ArgV]) ->
    ok = start(),
    not_done(run_local(ArgV));
do(["init"]) ->
    ok = compatibility_check([unix]),
    done(zx_local:initialize());
do(["list", "deps"]) ->
    done(zx_local:list_deps());
do(["list", "deps", PackageString]) ->
    done(zx_local:list_deps(PackageString));
do(["set", "dep", PackageString]) ->
    done(zx_local:set_dep(PackageString));
do(["drop", "dep", PackageString]) ->
    PackageID = zx_lib:package_id(PackageString),
    done(zx_local:drop_dep(PackageID));
do(["verup", Level]) ->
    ok = compatibility_check([unix]),
    done(zx_local:verup(Level));
do(["set", "version", VersionString]) ->
    ok = compatibility_check([unix]),
    done(zx_local:set_version(VersionString));
do(["update", ".app"]) ->
    done(zx_local:update_app_file());
do(["create", "plt"]) ->
    done(zx_local:create_plt());
do(["dialyze"]) ->
    done(zx_local:dialyze());
do(["package"]) ->
    {ok, TargetDir} = file:get_cwd(),
    done(zx_local:package(TargetDir));
do(["package", TargetDir]) ->
    case filelib:is_dir(TargetDir) of
        true  -> done(zx_local:package(TargetDir));
        false -> done({error, "Target directory does not exist", 22})
    end;
do(["submit", PackageFile]) ->
    done(zx_auth:submit(PackageFile));
do(["list", "pending", PackageName]) ->
    done(zx_auth:list_pending(PackageName));
do(["list", "approved", Realm]) ->
    done(zx_auth:list_approved(Realm));
do(["review", PackageString]) ->
    done(zx_auth:review(PackageString));
do(["approve", PackageString]) ->
    done(zx_auth:approve(PackageString));
do(["reject", PackageString]) ->
    done(zx_auth:reject(PackageString));
do(["add", "key"]) ->
    done(zx_auth:add_key());
do(["create", "user"]) ->
    done(zx_local:create_user());
do(["create", "userfile"]) ->
    done(zx_local:create_userfile());
do(["create", "keypair"]) ->
    done(zx_local:grow_a_pair());
do(["export", "user"]) ->
    done(zx_local:export_user());
do(["import", "user", ZdufFile]) ->
    done(zx_local:import_user(ZdufFile));
do(["list", "packagers", PackageName]) ->
    done(zx_auth:list_packagers(PackageName));
do(["list", "maintainers", PackageName]) ->
    done(zx_auth:list_maintainers(PackageName));
do(["list", "sysops", Realm]) ->
    done(zx_auth:list_sysops(Realm));
do(["create", "realmfile"]) ->
    done(zx_local:create_realmfile());
do(["install", PackageFile]) ->
    case filelib:is_regular(PackageFile) of
        true ->
            ok = start(),
            done(zx_daemon:install(PackageFile));
        false ->
            done({error, "Target directory does not exist", 22})
    end;
do(["accept", PackageString]) ->
    done(zx_auth:accept(PackageString));
do(["add", "package", PackageName]) ->
    done(zx_auth:add_package(PackageName));
do(["list", "users", Realm]) ->
    done(zx_auth:list_users(Realm));
do(["add", "user", ZpuFile]) ->
    done(zx_auth:add_user(ZpuFile));
do(["rem", "user", ZpuFile]) ->
    done(zx_auth:rem_user(ZpuFile));
do(["add", "packager", Package, UserName]) ->
    done(zx_auth:add_packager(Package, UserName));
do(["rem", "packager", Package, UserName]) ->
    done(zx_auth:rem_packager(Package, UserName));
do(["add", "maintainer", Package, UserName]) ->
    done(zx_auth:add_maintainer(Package, UserName));
do(["rem", "maintainer", Package, UserName]) ->
    done(zx_auth:rem_maintainer(Package, UserName));
do(["add", "sysop", Package, UserName]) ->
    done(zx_auth:add_sysop(Package, UserName));
do(["create", "realm"]) ->
    done(zx_local:create_realm());
do(["takeover", Realm]) ->
    done(zx_local:takeover(Realm));
do(["abdicate", Realm]) ->
    done(zx_local:abdicate(Realm));
do(_) ->
    usage_exit(22).


-spec done(outcome()) -> no_return().

done(ok) ->
    halt(0);
done({error, Code}) when is_integer(Code) ->
    ok = log(error, "Operation failed with code: ~w", [Code]),
    halt(Code);
done({error, Reason}) ->
    ok = log(error, "Operation failed with: ~160tp", [Reason]),
    halt(1);
done({error, Info, Code}) ->
    ok = log(error, Info),
    halt(Code).


-spec not_done(outcome()) -> ok | no_return().

not_done(ok)    -> ok;
not_done(Error) -> done(Error).


-spec compatibility_check(Platforms) -> ok | no_return()
    when Platforms :: unix | win32.
%% @private
%% Some commands only work on specific platforms because they leverage some specific
%% aspect on that platform, but not common to all. ATM this is mostly developer
%% commands that leverage things universal to *nix/posix shells but not Windows.
%% If equivalent procedures are written in Erlang then these restrictions can be
%% avoided -- but it is unclear whether there are any Erlang developers even using
%% Windows, so for now this is the bad version of the solution.

compatibility_check(Platforms) ->
    {Family, Name} = os:type(),
    case lists:member(Family, Platforms) of
        true ->
            ok;
        false ->
            Message = "Unfortunately this command is not available on ~tw ~tw",
            ok = log(error, Message, [Family, Name]),
            halt(0)
    end.



%%% Application Start/Stop

-spec start() -> ok | {error, Reason :: term()}.
%% @doc
%% An alias for `application:ensure_started(zx)', meaning it is safe to call this
%% function more than once, or within a system where you are unsure whether zx is
%% already running (perhaps due to complex dependencies that require zx already).
%% In the typical case this function does not ever need to be called, because the
%% zx_daemon is always started in the background whenever an application is started
%% using the command `zx run [app_id]'.
%% @equiv application:ensure_started(zx).

start() ->
%   ok = application:ensure_started(sasl),
    ok = application:ensure_started(zx),
    zx_daemon:init_connections().


-spec stop() -> ok | {error, Reason :: term()}.
%% @doc
%% A safe wrapper for `application:stop(zx)'. Similar to `ensure_started/1,2', returns
%% `ok' in the case that zx is already stopped.

stop() ->
    case application:stop(zx) of
        ok                         -> ok;
        {error, {not_started, zx}} -> ok;
        Error                      -> Error
    end.


%%% Application Callbacks

-spec start(StartType, StartArgs) -> Result
    when StartType :: normal,
         StartArgs :: none,
         Result    :: {ok, pid()}.
%% @private
%% Application callback. Not to be called directly.

start(normal, none) ->
    ok = application:ensure_started(inets),
    zx_sup:start_link().


-spec stop(term()) -> ok.
%% @private
%% Application callback. Not to be called directly.

stop(_) ->
    ok.



%%% Daemon Controls

-spec subscribe(package()) -> ok | {error, Reason :: term()}.
%% @doc
%% Initiates the zx_daemon and instructs it to subscribe to a package.
%%
%% Any events in the Zomp network that apply to the subscribed package will be
%% forwarded to the process that originally called subscribe/1. How the original
%% caller reacts to these notifications is up to the author -- not reply or "ack"
%% is expected.
%%
%% Package subscriptions can be used as the basis for user notification of updates,
%% automatic upgrade restarts, package catalog tracking, etc.

subscribe(Package) ->
    case application:start(?MODULE, normal) of
        ok    -> zx_daemon:subscribe(Package);
        Error -> Error
    end.


-spec unsubscribe() -> ok | {error, Reason :: term()}.
%% @doc
%% Unsubscribes from package updates.

unsubscribe() ->
    zx_daemon:unsubscribe().



%%% Execution of application

-spec run(PackageString, RunArgs) -> no_return()
    when PackageString :: string(),
         RunArgs       :: [string()].
%% @private
%% Given a program Identifier and a list of Args, attempt to locate the program and its
%% dependencies and run the program. This implies determining whether the program and
%% its dependencies are installed, available, need to be downloaded, or are inaccessible
%% given the current system condition (they could also be bogus, of course). The
%% Identifier must be a valid package string of the form `realm-appname[-version]'
%% where the `realm()' and `name()' must follow Zomp package naming conventions and the
%% version should be represented as a semver in string form (where ommitted elements of
%% the version always default to whatever is most current).
%%
%% Once the target program is running, this process, (which will run with the registered
%% name `zx') will sit in an `exec_wait' state, waiting for either a direct message from
%% a child program or for calls made via zx_lib to assist in environment discovery.
%%
%% If there is a problem anywhere in the locating, discovery, building, and loading
%% procedure the runtime will halt with an error message.

run(PackageString, RunArgs) ->
    case zx_lib:package_id(PackageString) of
        {ok, FuzzyID} -> run2(FuzzyID, RunArgs);
        Error         -> log(info, "run/2 got ~tp", [Error]), Error
    end.


run2(FuzzyID = {Realm, Name, _}, RunArgs) ->
    case resolve_installed_version(FuzzyID) of
        exact           -> run3(FuzzyID, RunArgs);
        {ok, Installed} -> run3({Realm, Name, Installed}, RunArgs);
        not_found       -> run3_maybe(FuzzyID, RunArgs)
    end.


run3_maybe(PackageID, RunArgs) ->
    {ok, ID} = zx_daemon:latest(PackageID),
    case wait_result(ID) of
        {ok, Version} ->
            NewID = setelement(3, PackageID, Version),
            {ok, PackageString} = zx_lib:package_string(NewID),
            ok = log(info, "Fetching ~ts", [PackageString]),
            run3_maybe2(NewID, RunArgs);
        Error ->
            Error
    end.

run3_maybe2(PackageID, RunArgs) ->
    case fetch(PackageID) of
        ok    -> run3(PackageID, RunArgs);
        Error -> Error
    end.


run3(PackageID, RunArgs) ->
    Dir = zx_lib:ppath(lib, PackageID),
    {ok, Meta} = zx_lib:read_project_meta(Dir),
    Type = maps:get(type, Meta),
    Deps = maps:get(deps, Meta),
    ok = prepare([PackageID | Deps]),
    execute(Type, PackageID, Meta, Dir, RunArgs).


-spec run_local(RunArgs) -> no_return()
    when RunArgs :: [term()].
%% @private
%% Execute a local project from source from the current directory, satisfying dependency
%% requirements via the locally installed zomp lib cache. The project must be
%% initialized as a zomp project (it must have a valid `zomp.meta' file).
%%
%% The most common use case for this function is during development. Using zomp support
%% via the local lib cache allows project authors to worry only about their own code
%% and use zx commands to add or drop dependencies made available via zomp.

run_local(RunArgs) ->
    {ok, Meta} = zx_lib:read_project_meta(),
    PackageID = {_, Name, _} = maps:get(package_id, Meta),
    Type = maps:get(type, Meta),
    Deps = maps:get(deps, Meta),
    {ok, Dir} = file:get_cwd(),
    true = os:putenv(Name ++ "_include", filename:join(Dir, "include")),
    case prepare(Deps) of
        ok ->
            ok = file:set_cwd(Dir),
            ok = zx_lib:build(),
            ok = file:set_cwd(zx_lib:zomp_dir()),
            execute(Type, PackageID, Meta, Dir, RunArgs);
        Error ->
            Error
    end.


-spec prepare([zx:package_id()]) -> ok.
%% @private
%% Execution prep common to all packages.

prepare(Deps) ->
    true = os:putenv("zx_include", filename:join(os:getenv("ZX_DIR"), "include")),
    ok = lists:foreach(fun include_env/1, Deps),
    NotInstalled = fun(P) -> not filelib:is_dir(zx_lib:ppath(lib, P)) end,
    Needed = lists:filter(NotInstalled, Deps),
    acquire(Needed, Deps).

acquire([Dep | Rest], Deps) ->
    case fetch(Dep) of
        ok    -> acquire(Rest, Deps);
        Error -> Error
    end;
acquire([], Deps) ->
    make(Deps).

make([Dep | Rest]) ->
    case zx_daemon:build(Dep) of
        ok    -> make(Rest);
        Error -> Error
    end;
make([]) ->
    log(info, "Deps prepared.").
    


include_env(PackageID = {_, Name, _}) ->
    Path = filename:join(zx_lib:ppath(lib, PackageID), "include"),
    os:putenv(Name ++ "_include", Path).


-spec fetch(zx:package_id()) -> zx:outcome().

fetch(PackageID) ->
    {ok, ID} = zx_daemon:fetch(PackageID),
    fetch2(ID).

fetch2(ID) ->
    receive
        {result, ID, done} ->
            ok;
        {result, ID, {hops, Count}} ->
            ok = log(info, "Inbound; ~w hops away.", [Count]),
            fetch2(ID);
        {result, ID, {error, Reason}} ->
            {error, Reason, 1}
        after 10000 ->
            {error, timeout, 62}
    end.


-spec execute(Type, PackageID, Meta, Dir, RunArgs) -> no_return()
    when Type      :: app | lib,
         PackageID :: package_id(),
         Meta      :: package_meta(),
         Dir       :: file:filename(),
         RunArgs   :: [string()].
%% @private
%% Gets all the target application's ducks in a row and launches them, then enters
%% the exec_wait/1 loop to wait for any queries from the application.

execute(app, PackageID, Meta, Dir, RunArgs) ->
    {ok, PackageString} = zx_lib:package_string(PackageID),
    ok = log(info, "Starting ~ts.", [PackageString]),
    Name = element(2, PackageID),
    ok = zx_daemon:pass_meta(Meta, Dir, RunArgs),
    AppTag = list_to_atom(Name),
    ok = ensure_all_started(AppTag),
    log(info, "Launcher complete.");
execute(lib, PackageID, _, _, _) ->
    Message = "Lib ~ts is available on the system, but is not a standalone app.",
    {ok, PackageString} = zx_lib:package_string(PackageID),
    ok = log(info, Message, [PackageString]),
    halt(0).


-spec ensure_all_started(AppMod) -> ok
    when AppMod :: module().
%% @private
%% Wrap a call to application:ensure_all_started/1 to selectively provide output
%% in the case any dependencies are actually started by the call. Might remove this
%% depending on whether SASL winds up becoming a standard part of the system and
%% whether it becomes common for dependencies to all signal their own start states
%% somehow.

ensure_all_started(AppMod) ->
    case application:ensure_all_started(AppMod) of
        {ok, []}   -> ok;
        {ok, Apps} -> log(info, "Started ~160tp", [Apps])
    end.


-spec resolve_installed_version(PackageID) -> Result
    when PackageID :: package_id(),
         Result    :: not_found
                    | exact
                    | {ok, Installed :: version()}.
%% @private
%% Resolve the provided PackageID to the latest matching installed package directory
%% version if one exists, returning a value that indicates whether an exact match was
%% found (in the case of a full version input), a version matching a partial version
%% input was found, or no match was found at all.

resolve_installed_version({Realm, Name, Version}) ->
    PackageDir = zx_lib:path(lib, Realm, Name),
    case filelib:is_dir(PackageDir) of
        true  -> resolve_installed_version(PackageDir, Version);
        false -> not_found
    end.


resolve_installed_version(PackageDir, Version) ->
    DirStrings = filelib:wildcard("*", PackageDir),
    Versions = lists:foldl(fun tuplize/2, [], DirStrings),
    zx_lib:find_latest_compatible(Version, Versions).


tuplize(String, Acc) ->
    case zx_lib:string_to_version(String) of
        {ok, Version} -> [Version | Acc];
        _             -> Acc
    end.


wait_result(ID) ->
    receive
        {result, ID, Result} -> Result
        after 5000           -> {error, timeout}
    end.



%%% Usage

-spec usage_exit(Code) -> no_return()
    when Code :: integer().
%% @private
%% A convenience function that will display the zx usage message before halting
%% with the provided exit code.

usage_exit(Code) ->
    ok = lists:foreach(fun io:format/1, usage_all()),
    halt(Code).


help(top)   -> show_help();
help(user)  -> show_help([usage_header(), usage_user(), usage_spec()]);
help(dev)   -> show_help([usage_header(), usage_dev(), usage_spec()]);
help(sysop) -> show_help([usage_header(), usage_sysop(), usage_spec()]).


show_help() ->
    T =
        "ZX help has three forms, one for each category of commands:~n"
        "    zx help [user | dev | sysop]~n"
        "The user manual is also available online at: http://zxq9.com/projects/zomp/~n",
    io:format(T).


show_help(Info) -> lists:foreach(fun io:format/1, Info).


usage_all() ->
    [usage_header(), usage_user(), usage_dev(), usage_sysop(), usage_spec()].

usage_header() ->
    "ZX usage: zx [command] [object] [args]~n~n".

usage_user() ->
    "User Actions:~n"
    "  zx help~n"
    "  zx run PackageID [Args]~n"
    "  zx list realms~n"
    "  zx list packages Realm~n"
    "  zx list versions PackageID~n"
    "  zx latest PackageID~n"
    "  zx import realm RealmFile~n"
    "  zx drop realm Realm~n"
    "  zx logpath [Package [1-10]]~n"
    "  zx status~n"
    "  zx set timeout Value~n"
    "  zx add mirror Realm Host:Port~n"
    "  zx drop mirror Realm Host:Port~n~n".

usage_dev() ->
    "Developer/Packager/Maintainer Actions:~n"
    "  zx create project~n"
    "  zx runlocal [Args]~n"
    "  zx init~n"
    "  zx list deps [PackageID]~n"
    "  zx set dep PackageID~n"
    "  zx drop dep PackageID~n"
    "  zx verup Level~n"
    "  zx set version Version~n"
    "  zx update .app~n"
    "  zx create plt~n"
    "  zx dialyze~n"
    "  zx package Path~n"
    "  zx submit ZSP~n"
    "  zx list pending PackageName~n"
    "  zx review PackageID~n"
    "  zx approve PackageID~n"
    "  zx reject PackageID~n"
    "  zx add key~n"
    "  zx create user~n"
    "  zx create userfile~n"
    "  zx create keypair~n"
    "  zx export user~n"
    "  zx import user [ZPUF | ZDUF]~n"
    "  zx list packagers PackageName~n"
    "  zx list maintainers PackageName~n"
    "  zx list sysops Realm~n"
    "  zx install ZSP~n~n".

usage_sysop() ->
    "Sysop Actions:~n"
    "  zx list approved Realm~n"
    "  zx accept PackageID~n"
    "  zx add package PackageName~n"
    "  zx list users Realm~n"
    "  zx add user ZPUF~n"
    "  zx add packager PackageName UserID~n"
    "  zx add maintainer PackageName UserID~n"
    "  zx add sysop UserID~n"
    "  zx create realm~n"
    "  zx create realmfile~n"
    "  zx takeover Realm~n"
    "  zx abdicate Realm~n~n".

usage_spec() ->
    "Where~n"
    "  PackageID :: A string of the form Realm-Name[-Version]~n"
    "  Args      :: Arguments to pass to the application~n"
    "  Type      :: The project type: a standalone \"app\" or a \"lib\"~n"
    "  Version   :: Version string X, X.Y, or X.Y.Z: \"1\", \"1.2\", \"1.2.3\"~n"
    "  RealmFile :: Path to a valid .zrf realm file~n"
    "  Realm     :: The name of a realm as a string [:a-z:]~n"
    "  KeyName   :: The prefix of a keypair to drop~n"
    "  Level     :: The version level, one of \"major\", \"minor\", or \"patch\"~n"
    "  Path      :: Path or filename.~n"
    "  ZSP       :: Path to a .zsp file (Zomp Source Package).~n"
    "  ZPUF      :: Path to a .zpuf file (Zomp Public User File).~n"
    "  ZDUF      :: Path to a .zduf file (Zomp DANGEROUS User File).~n".
