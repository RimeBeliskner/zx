-export([log/2, log/3]).


-spec log(Level, Format) -> ok
    when Level  :: info
                 | warning
                 | error,
         Format :: string().
%% @private
%% @equiv log(Level, Format, [])

log(Level, Format) ->
    log(Level, Format, []).


-spec log(Level, Format, Args) -> ok
    when Level  :: info
                 | warning
                 | error,
         Format :: string(),
         Args   :: [term()].
%% @private
%% A logging abstraction to hide whatever logging back end is actually in use.
%% Format must adhere to Erlang format string rules, and the arity of Args must match
%% the provided format.

log(Level, Format, Args) ->
    Tag =
        case Level of
            info    -> "[INFO]";
            warning -> "[WARNING]";
            error   -> "[ERROR]"
        end,
    Out = io_lib:format("~s ~w ~w: " ++ Format ++ "~n", [Tag, ?MODULE, self() | Args]),
    UTF8 = unicode:characters_to_binary(Out),
    io:format(UTF8).
