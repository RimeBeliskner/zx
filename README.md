# ZX: The Zomp client
TLDR: If you have ZX on your system, "zx run [program name]" will launch any Erlang-based program that exists in the Zomp system.
"zx create project [foo]" will start a project for you.
Read the [docs](http://zxq9.com/projects/zomp/) for more.

Zomp is a from-source code repository and distributed distribution system.
ZX is a front-end for that system that incorporates developer tools with end-user launching functionality.

## Docs
http://zxq9.com/projects/zomp/

## Code
https://gitlab.com/zxq9/zx/
