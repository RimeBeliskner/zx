#! /usr/bin/env escript

%% ZX install script

-mode(compile).

-spec main(Argv :: [string()]) -> no_return().

main(_) ->
    OS = os:type(),
    ZompDir = zomp_dir(OS),
    case filelib:is_dir(ZompDir) of
        true ->
            Message = "Installation directory (~ts) already exists. Aborting.~n",
            ok = io:format(Message, [ZompDir]),
            halt(0);
        false ->
            unpack(OS, ZompDir)
    end.


-spec unpack(OS, ZompDir) -> no_return()
    when OS      :: {Family :: unix | win32,
                     Name   :: atom()},
         ZompDir :: file:filename().

unpack(OS, ZompDir) ->
    BaseDir = filename:dirname(ZompDir),
    ok = erl_tar:extract("zomp.tar.gz", [compressed, {cwd, BaseDir}]),
    add_launcher(OS, ZompDir).


-spec add_launcher(OS, ZompDir) -> no_return()
    when OS      :: {Family :: unix | win32,
                     Name   :: atom()},
         ZompDir :: file:filename().

add_launcher({unix, linux}, ZompDir) ->
    add_unix_link(ZompDir),
    ok = io:format("Should attempt a FreeDesktop icon addition here.~n"),
    halt(0);
add_launcher({unix, _}, ZompDir) ->
    add_unix_link(ZompDir),
    halt(0);
add_launcher({win32, nt}, _ZompDir) ->
    ok = io:format("This is going to be a bit of a rodeo...~n"),
    halt(0).


-spec add_unix_link(ZompDir) -> ok
    when ZompDir :: file:filename().

add_unix_link(ZompDir) ->
    Home = filename:dirname(ZompDir),
    Link = filename:join(Home, "bin/zx"),
    LinkH = filename:join(Home, "bin/zxh"),
    HomeBin = filename:dirname(Link),
    Target = filename:join(ZompDir, "zx"),
    TargetH = filename:join(ZompDir, "zxh"),
    ok = filelib:ensure_dir(Link),
    LinkCommand = "env LANG=en ln -s " ++ Target ++ " " ++ Link,
    LinkCommandH = "env LANG=en ln -s " ++ TargetH ++ " " ++ LinkH,
    ModeCommand = "env LANG=en chmod +x " ++ Target,
    ModeCommandH = "env LANG=en chmod +x " ++ TargetH,
    ok = os_cmd(LinkCommand),
    ok = os_cmd(LinkCommandH),
    ok = os_cmd(ModeCommand),
    ok = os_cmd(ModeCommandH),
    Path = os:getenv("PATH"),
    Parts = string:lexemes(Path, ":"),
    Message =
        case lists:member(HomeBin, Parts) of
            true ->
                "A link to ~ts has been created at ~ts.~n"
                "~ts seems to be in $PATH already.~n"
                "You should be able to run any Zomp/ZX program as a normal shell "
                "command by typing `zx`, or with an Erlang shell attached by "
                "typing `zxh` from anywhere in your system.~n"
                "Creating launchers to the link that start specific programs may be "
                "conveinent.~n"
                "A desktop launcher would need to run the command:~n"
                "    zx run [some_program]~n"
                "Or alternately (if ~~/bin/ is not in your launcher's $PATH):~n"
                "    ~ts run [some_program]~n";
            false ->
                "A link to ~ts has been created at ~ts. "
                "~ts seems to be NOT in your $PATH. "
                "You will need to add that to $PATH if you want to be able to run "
                "zx by simply typing `zx` (or zxh to attach an Erlang shell) from "
                "anywhere in your system.~n"
                "Creating launchers to the link that start specific programs may be "
                "conveinent.~n"
                "A desktop launcher would need to run the command:~n"
                "    ~ts run [some_program]~n"
        end,
    io:format(Message, [Target, Link, HomeBin, Target]).


-spec os_cmd(Command :: string()) -> ok.

os_cmd(Command) ->
    case os:cmd(Command) of
        "" ->
            ok;
        Output ->
            ok = io:format("Output of: ~tp~n", [Command]),
            io:format("~ts~n", [Output])
    end.


-spec zomp_dir(OS) -> file:filename()
    when OS :: {Family :: unix | win32,
                Name   :: atom()}.
%% @private
%% Check the host OS and return the absolute path to the zomp filesystem root.

zomp_dir({unix, _}) ->
    Home = os:getenv("HOME"),
    filename:join(Home, ".zomp");
zomp_dir({win32, _}) ->
    Path = os:getenv("LOCALAPPDATA"),
    filename:join(Path, "zomp");
zomp_dir(Unknown) ->
    Message = "zx_install: ERROR Unknown host system type: ~tp",
    ok = io:format(Message, [Unknown]),
    halt(1).
