@echo off
setlocal

set versions=9.2

for %%v in (%versions%) do (
    if exist "%ProgramFiles%\erl%%v\bin\escript.exe" (
        "%ProgramFiles%\erl%%v\bin\escript.exe" install.escript
        notify "Done!"
        exit /b
    )
)

echo Could not find the Erlang escript.exe executable
notify "Could not locate a expected Erlang runtime. Consult the README.windows file in this directory for more information."
exit /b